import Config

database =
  if config_env() == :test do
    "anubis_test#{System.get_env("MIX_TEST_PARTITION")}"
  else
    System.get_env("DB_DATABASE") || raise "DB_DATABASE not set"
  end

config :anubis, Anubis.Repo,
  username: System.get_env("DB_USERNAME") || raise("DB_USERNAME not set"),
  password: System.get_env("DB_PASSWORD") || raise("DB_PASSWORD not set"),
  hostname: System.get_env("DB_HOSTNAME") || raise("DB_HOSTNAME not set"),
  database: database,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

config :anubis,
  fmp_token: System.get_env("FMP_TOKEN"),
  bls_api_key: System.get_env("BLS_API_KEY")
