use Mix.Config

config :anubis, AnubisWeb.Endpoint,
  url: [host: "anubis.abstractbinary.org", port: 80],
  cache_static_manifest: "priv/static/cache_manifest.json"

config :logger, level: :info

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :anubis, AnubisWeb.Endpoint,
  server: true,
  http: [
    port: String.to_integer(System.get_env("PORT") || "4000"),
    transport_options: [socket_opts: [:inet6]]
  ],
  secret_key_base: secret_key_base
