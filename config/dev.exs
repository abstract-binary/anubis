use Mix.Config

esbuild = Path.expand("../assets/node_modules/.bin/esbuild", __DIR__)

config :anubis, Anubis.Repo,
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with webpack to recompile .js and .css sources.
config :anubis, AnubisWeb.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [
    "#{esbuild}": [
      "./js/app.js",
      "--target=es2015",
      "--bundle",
      "--outdir=../priv/static/js",
      "--sourcemap",
      "--watch",
      cd: Path.expand("../assets", __DIR__)
    ],
    node: [
      "./node_modules/.bin/postcss",
      "./css/*.css",
      "./node_modules/katex/dist/katex.css",
      "--dir",
      "../priv/static/css",
      "-w",
      cd: Path.expand("../assets", __DIR__),
      env: [{"NODE_ENV", "development"}, {"TAILWIND_MODE", "watch"}]
    ],
    node: [
      "./node_modules/.bin/cpx",
      "'static/**/*'",
      "../priv/static",
      "--watch",
      cd: Path.expand("../assets", __DIR__)
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Initialize plugs at runtime for faster development compilation
config :phoenix, :plug_init_mode, :runtime
