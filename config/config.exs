# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :anubis,
  ecto_repos: [Anubis.Repo],
  fmp_url: "https://financialmodelingprep.com"

# Configures the endpoint
config :anubis, AnubisWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "QVGb28Wfu/eNzBV7yn0np0tNCAMmUzTyWMjY13567zm/zaMXYF4p946pDZme0yYe",
  render_errors: [view: AnubisWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Anubis.PubSub,
  live_view: [signing_salt: "D90zWNMn"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
