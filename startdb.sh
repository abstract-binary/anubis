#!/bin/bash

set -e -o pipefail

if [[ ! -d data ]]; then
    echo "Creating database"
    mkdir data
    pg_ctl initdb -D data
    pg_ctl -w -D data -o '-k /tmp' start
    psql -U $(whoami) -d postgres -c "CREATE DATABASE ${DB_DATABASE};"
else
    pg_ctl -w -D data -o '-k /tmp' start
fi

# docker run \
#        --name anubisdb \
#        -p 5432:5432 \
#        -e POSTGRES_DB=${DB_DATABASE} \
#        -e POSTGRES_USER=${DB_USERNAME} \
#        -e POSTGRES_PASSWORD=${DB_PASSWORD} \
#        -e PGDATA=/var/lib/postgresql/data \
#        -v $(pwd)/data:/var/lib/postgresql/data docker.io/library/postgres:13.2 \
#     || docker start anubisdb
