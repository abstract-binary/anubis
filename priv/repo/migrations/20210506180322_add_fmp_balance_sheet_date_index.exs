defmodule Anubis.Repo.Migrations.AddFmpBalanceSheetDateIndex do
  use Ecto.Migration

  def up do
    execute(
      "CREATE INDEX fmp_balance_sheet_statements_date_index on fmp_balance_sheet_statements USING BTREE ((data->>'date'))"
    )
  end

  def down do
    execute("DROP INDEX fmp_balance_sheet_statements_date_index")
  end
end
