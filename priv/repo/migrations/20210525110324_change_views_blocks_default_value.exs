defmodule Anubis.Repo.Migrations.ChangeViewsBlocksDefaultValue do
  use Ecto.Migration

  def up do
    alter table(:views) do
      modify :blocks, :map, null: false, default: fragment("'[]'")
    end
  end

  def down do
    alter table(:views) do
      modify :blocks, :map, null: false
    end
  end
end
