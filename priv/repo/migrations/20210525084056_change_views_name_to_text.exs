defmodule Anubis.Repo.Migrations.ChangeViewsNameToText do
  use Ecto.Migration

  def change do
    alter table(:views) do
      modify :name, :text, null: false
    end
  end
end
