defmodule Anubis.Repo.Migrations.ChangeFieldKinds do
  use Ecto.Migration

  def up do
    execute("DELETE FROM field_kinds")
    execute("INSERT INTO field_kinds VALUES ('scalar')")
    execute("INSERT INTO field_kinds VALUES ('year_series')")
  end

  def down do
    execute("DELETE FROM field_kinds")
    execute("INSERT INTO field_kinds VALUES ('basic')")
    execute("INSERT INTO field_kinds VALUES ('elixir')")
    execute("INSERT INTO field_kinds VALUES ('formula')")
  end
end
