defmodule Anubis.Repo.Migrations.AddStatsMiscYearAndPeriodCols do
  use Ecto.Migration

  def up do
    execute("DROP INDEX stats_misc_period_index")
    execute("DROP INDEX stats_misc_year_index")

    alter table(:stats_misc) do
      add :year, :integer, null: false
      add :period, :text, null: false
    end

    create index(:stats_misc, [:series, :year, :period], unique: true)

    execute("CREATE INDEX stats_misc_year_index ON stats_misc USING BTREE (year)")
    execute("CREATE INDEX stats_misc_period_index ON stats_misc USING BTREE (period)")
  end

  def down do
    execute("DROP INDEX stats_misc_period_index")
    execute("DROP INDEX stats_misc_year_index")

    drop index(:stats_misc, [:series, :year, :period], unique: true)

    alter table(:stats_misc) do
      remove :year
      remove :period
    end

    execute("CREATE INDEX stats_misc_year_index ON stats_misc USING BTREE ((data->>'year'))")
    execute("CREATE INDEX stats_misc_period_index ON stats_misc USING BTREE ((data->>'period'))")
  end
end
