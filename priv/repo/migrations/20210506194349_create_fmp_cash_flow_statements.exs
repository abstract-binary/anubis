defmodule Anubis.Repo.Migrations.CreateFmpCashFlowStatements do
  use Ecto.Migration

  def up do
    create table(:fmp_cash_flow_statements) do
      add :ticker, :text, null: false
      add :data, :jsonb, null: false

      timestamps()
    end

    execute(
      "CREATE INDEX fmp_cash_flow_statements_date_index on fmp_cash_flow_statements USING BTREE ((data->>'date'))"
    )
  end

  def down do
    execute("DROP INDEX fmp_cash_flow_statements_date_index")
    execute("DROP TABLE fmp_cash_flow_statements")
  end
end
