defmodule Anubis.Repo.Migrations.AddFmpTickersTsvectorIndex do
  use Ecto.Migration

  def up do
    execute("CREATE EXTENSION pg_trgm")
    execute("CREATE INDEX fmp_tickers_text_search_index ON fmp_tickers \
USING GIN ((ticker || ' ' || name || ' ' || exchange) gin_trgm_ops)")
  end

  def down do
    execute("DROP INDEX fmp_tickers_text_search_index")
    execute("DROP EXTENSION pg_trgm")
  end
end
