defmodule Anubis.Repo.Migrations.AddFieldKind do
  use Ecto.Migration

  def up do
    create table(:field_kinds, primary_key: false) do
      add :kind, :text, primary_key: true
    end

    execute("INSERT INTO field_kinds VALUES ('basic')")
    execute("INSERT INTO field_kinds VALUES ('elixir')")
    execute("INSERT INTO field_kinds VALUES ('formula')")
  end

  def down do
    drop table(:field_kinds)
  end
end
