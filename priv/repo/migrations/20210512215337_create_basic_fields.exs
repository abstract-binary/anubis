defmodule Anubis.Repo.Migrations.CreateBasicFields do
  use Ecto.Migration

  def change do
    create table(:basic_fields, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :text, null: false
      add :description, :text, null: false
      add :accessor, :text, null: false

      add :table, references(:raw_tables, on_delete: :nothing, type: :text, column: "table"),
        null: false

      timestamps()
    end

    create index(:basic_fields, [:table])
  end
end
