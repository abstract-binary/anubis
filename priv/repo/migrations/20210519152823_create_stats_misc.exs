defmodule Anubis.Repo.Migrations.CreateStatsMisc do
  use Ecto.Migration

  def up do
    create table(:stats_misc) do
      add :series, :text, null: false
      add :data, :jsonb, null: false

      timestamps()
    end

    execute("CREATE INDEX stats_misc_year_index ON stats_misc USING BTREE ((data->>'year'))")

    execute("CREATE INDEX stats_misc_period_index ON stats_misc USING BTREE ((data->>'period'))")
  end

  def down do
    execute("DROP INDEX stats_misc_period_index")
    execute("DROP INDEX stats_misc_year_index")
    execute("DROP TABLE stats_misc")
  end
end
