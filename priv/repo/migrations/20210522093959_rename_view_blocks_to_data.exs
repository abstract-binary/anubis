defmodule Anubis.Repo.Migrations.RenameViewBlocksToData do
  use Ecto.Migration

  def change do
    rename table(:views), :blocks, to: :data
  end
end
