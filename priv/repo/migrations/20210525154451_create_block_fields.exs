defmodule Anubis.Repo.Migrations.CreateBlockFields do
  use Ecto.Migration

  def change do
    create table(:block_basic_fields, primary_key: false) do
      add :block_id, references("blocks", type: :binary_id), primary_key: true
      add :basic_field_id, references("basic_fields", type: :binary_id), primary_key: true
    end

    create index(:block_basic_fields, [:block_id])
    create index(:block_basic_fields, [:basic_field_id])

    create table(:block_calc_fields, primary_key: false) do
      add :block_id, references("blocks", type: :binary_id), primary_key: true
      add :calc_field_id, references("calc_fields", type: :binary_id), primary_key: true
    end

    create index(:block_calc_fields, [:block_id])
    create index(:block_calc_fields, [:calc_field_id])
  end
end
