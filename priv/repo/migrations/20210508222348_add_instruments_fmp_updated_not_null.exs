defmodule Anubis.Repo.Migrations.AddInstrumentsFmpUpdatedNotNull do
  use Ecto.Migration

  def change do
    alter table("instruments") do
      modify :fmp_updated, :naive_datetime, null: false
    end
  end
end
