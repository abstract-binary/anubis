defmodule Anubis.Repo.Migrations.CreateCalcFields do
  use Ecto.Migration

  def change do
    create table(:calc_fields, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :text, null: false
      add :description, :text, null: false
      add :formula, :jsonb, null: false

      timestamps()
    end
  end
end
