defmodule Anubis.Repo.Migrations.CreateViews do
  use Ecto.Migration

  def change do
    create table(:views, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string, null: false
      add :blocks, :json, null: false

      timestamps()
    end
  end
end
