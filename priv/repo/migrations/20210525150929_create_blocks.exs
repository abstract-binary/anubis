defmodule Anubis.Repo.Migrations.CreateBlocks do
  use Ecto.Migration

  def up do
    create table(:blocks, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :text, :text, null: false, default: ""
      add :title, :text, null: false
      add :view_id, references("views", type: :binary_id)

      timestamps()
    end

    alter table(:views) do
      remove :blocks
    end
  end

  def down do
    drop table(:blocks)

    alter table(:views) do
      add :blocks, :map
    end
  end
end
