defmodule Anubis.Repo.Migrations.CreateFmpIncomeStatements do
  use Ecto.Migration

  def up do
    create table(:fmp_income_statements) do
      add :ticker, :text, null: false
      add :data, :jsonb, null: false

      timestamps()
    end

    execute(
      "CREATE INDEX fmp_income_statements_date_index on fmp_income_statements USING BTREE ((data->>'date'))"
    )
  end

  def down do
    execute("DROP INDEX fmp_income_statements_date_index")
    execute("DROP TABLE fmp_income_statements")
  end
end
