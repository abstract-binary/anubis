defmodule Anubis.Repo.Migrations.AddInstrumentsFmpUpdated do
  use Ecto.Migration

  def change do
    alter table("instruments") do
      add :fmp_updated, :naive_datetime
    end
  end
end
