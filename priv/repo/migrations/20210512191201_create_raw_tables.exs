defmodule Anubis.Repo.Migrations.CreateRawTables do
  use Ecto.Migration

  def change do
    create table(:raw_tables, primary_key: false) do
      add :table, :text, primary_key: true
      add :description, :text, null: false
      add :module, :text, null: false
      add :function, :text, null: false

      timestamps()
    end
  end
end
