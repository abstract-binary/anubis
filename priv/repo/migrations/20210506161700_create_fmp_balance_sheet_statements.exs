defmodule Anubis.Repo.Migrations.CreateFmpBalanceSheetStatements do
  use Ecto.Migration

  def change do
    create table(:fmp_balance_sheet_statements) do
      add :ticker, :text, null: false
      add :data, :jsonb, null: false

      timestamps()
    end
  end
end
