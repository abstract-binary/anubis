defmodule Anubis.Repo.Migrations.CreateInstruments do
  use Ecto.Migration

  def change do
    create table(:instruments) do
      add :symbol, :text, null: false
      add :fmp_ticker, :text

      timestamps()
    end
  end
end
