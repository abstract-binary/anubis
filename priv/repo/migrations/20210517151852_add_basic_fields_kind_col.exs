defmodule Anubis.Repo.Migrations.AddBasicFieldsKindCol do
  use Ecto.Migration

  def up do
    alter table("basic_fields") do
      add :kind, references(:field_kinds, on_delete: :nothing, type: :text, column: "kind")
    end

    execute("UPDATE basic_fields SET kind = 'year_series'")

    alter table("basic_fields") do
      modify :kind, :text, null: false
    end
  end

  def down do
    alter table("basic_fields") do
      drop :kind
    end
  end
end
