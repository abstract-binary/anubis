defmodule Anubis.Repo.Migrations.CreateTickers do
  use Ecto.Migration

  def change do
    create table(:fmp_tickers, primary_key: false) do
      add :ticker, :text, null: false, primary_key: true
      add :name, :text, null: false
      add :exchange, :text, null: false

      timestamps()
    end
  end
end
