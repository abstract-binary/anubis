defmodule Anubis.Repo.Migrations.ChangeViewsBlocksToEmbeddedSchema do
  use Ecto.Migration

  def change do
    rename table(:views), :data, to: :blocks

    alter table(:views) do
      modify :blocks, :map, null: false
    end
  end
end
