defmodule Anubis.Repo.Migrations.AddBlockFieldsMetaColumns do
  use Ecto.Migration

  def change do
    alter table(:block_basic_fields) do
      add :order, :integer, null: false, default: 50
    end

    alter table(:block_calc_fields) do
      add :order, :integer, null: false, default: 50
    end
  end
end
