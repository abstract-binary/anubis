#!/bin/bash

set -e -o pipefail

if [[ -d data ]]; then
    pg_ctl -D data stop
fi
