defmodule AnubisWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use AnubisWeb, :controller
      use AnubisWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  @spec controller() :: Macro.t()
  def controller do
    quote do
      use Phoenix.Controller, namespace: AnubisWeb

      import Plug.Conn
      import AnubisWeb.Gettext
      alias AnubisWeb.Router.Helpers, as: Routes
    end
  end

  @spec view() :: Macro.t()
  def view do
    quote do
      use Phoenix.View,
        root: "lib/anubis_web/templates",
        namespace: AnubisWeb

      # Import convenience functions from controllers
      import Phoenix.Controller,
        only: [get_flash: 1, get_flash: 2, view_module: 1, view_template: 1]

      # Include shared imports and aliases for views
      unquote(view_helpers())
    end
  end

  @spec live_view() :: Macro.t()
  def live_view do
    quote do
      use Phoenix.LiveView,
        layout: {AnubisWeb.LayoutView, "live.html"}

      unquote(view_helpers())
    end
  end

  @spec live_component() :: Macro.t()
  def live_component do
    quote do
      use Phoenix.LiveComponent

      unquote(view_helpers())
    end
  end

  @spec router() :: Macro.t()
  def router do
    quote do
      use Phoenix.Router

      import Plug.Conn
      import Phoenix.Controller
      import Phoenix.LiveView.Router
    end
  end

  @spec channel() :: Macro.t()
  def channel do
    quote do
      use Phoenix.Channel
      import AnubisWeb.Gettext
    end
  end

  @spec helper_module() :: Macro.t()
  def helper_module do
    quote do
      unquote(view_helpers())
    end
  end

  @spec view_helpers() :: Macro.t()
  defp view_helpers do
    quote do
      # Import basic rendering functionality (render, render_layout, etc)
      import Phoenix.View

      # Import LiveView helpers (live_render, live_component, live_patch, etc)
      import Phoenix.LiveView.Helpers

      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML

      import AnubisWeb.ErrorHelpers
      import AnubisWeb.Gettext
      import AnubisWeb.LiveHelpers
      alias AnubisWeb.Router.Helpers, as: Routes
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
