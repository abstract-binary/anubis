defmodule AnubisWeb.ViewLive.BlockFormComponent do
  @moduledoc """
  Live component for editing the details of individual blocks.
  """

  use AnubisWeb, :live_component

  alias Anubis.Viewer
  alias Anubis.Viewer.Block

  require Logger

  @impl Phoenix.LiveComponent
  def update(%{block_id: block_id, view: view} = assigns, socket) do
    view_changeset = Viewer.change_view(view)
    block = Enum.find(view.blocks, &(&1.id == block_id)) || %Block{}
    changeset = Viewer.change_block(block)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:block, block)
     |> assign(:changeset, changeset)
     |> assign(:view_changeset, view_changeset)}
  end

  @impl Phoenix.LiveComponent
  def handle_event("validate", %{"block" => block_params}, socket) do
    changeset =
      socket.assigns.block
      |> Viewer.change_block(block_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"block" => block_params}, socket) do
    save_block(socket, socket.assigns.action, block_params)
  end

  defp save_block(socket, :edit_block, block_params) do
    blocks =
      Enum.map(socket.assigns.view.blocks, fn block ->
        if block.id == socket.assigns.block.id do
          Viewer.change_block(block, block_params)
        else
          block
        end
      end)

    case Viewer.update_view_blocks(socket.assigns.view, blocks) do
      {:ok, _view} ->
        {:noreply,
         socket
         |> put_flash(:info, "Block updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, changeset} ->
        Logger.error("Error updating block: #{inspect(changeset)}")
        {:noreply, assign(socket, :view_changeset, changeset)}
    end
  end

  defp save_block(socket, :new_block, block_params) do
    blocks =
      socket.assigns.view.blocks ++ [Viewer.change_block(socket.assigns.block, block_params)]

    case Viewer.update_view_blocks(socket.assigns.view, blocks) do
      {:ok, _view} ->
        {:noreply,
         socket
         |> put_flash(:info, "Block created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, changeset} ->
        Logger.error("Error creating block: #{inspect(changeset)}")
        {:noreply, assign(socket, :view_changeset, changeset)}
    end
  end
end
