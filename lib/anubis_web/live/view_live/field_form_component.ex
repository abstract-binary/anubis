defmodule AnubisWeb.ViewLive.FieldFormComponent do
  @moduledoc """
  Live component for editing the a field as referenced by a block.
  """

  use AnubisWeb, :live_component

  alias Anubis.Synth
  alias Anubis.Synth.BasicField
  alias Anubis.Viewer
  alias Anubis.Viewer.{BlockBasicField, BlockCalcField}

  require Logger

  @impl Phoenix.LiveComponent
  def update(%{field: field, block: block} = assigns, socket) do
    basic_fields = Synth.list_basic_fields()
    calc_fields = Synth.list_calc_fields()

    field = field || %BlockBasicField{basic_field: %BasicField{}}

    field_type =
      case field do
        %BlockBasicField{} -> :basic
        %BlockCalcField{} -> :calc
      end

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:block, block)
     |> assign(:field, field)
     |> assign(:basic_fields, basic_fields)
     |> assign(:calc_fields, calc_fields)
     |> assign(:field_type, field_type)}
  end

  @impl Phoenix.LiveComponent
  def handle_event("changed", %{"field_form" => %{"field_type" => field_type}}, socket) do
    {:noreply, assign(socket, :field_type, String.to_existing_atom(field_type))}
  end

  @impl Phoenix.LiveComponent
  def handle_event("save", %{"field_form" => form_params}, socket) do
    save_block_basic_field(socket, socket.assigns.action, form_params)
  end

  defp save_block_basic_field(socket, :block_edit_field, form_params) do
    field_type = form_params["field_type"] |> String.to_existing_atom()
    field_order = form_params["field_order"] |> String.to_integer()

    form_accessor =
      case field_type do
        :basic -> "basic_field_id"
        :calc -> "calc_field_id"
      end

    Viewer.update_block_field(
      socket.assigns.block,
      field_type,
      form_params[form_accessor],
      field_order
    )
    |> case do
      :ok ->
        {:noreply,
         socket
         |> put_flash(:info, "Block field updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, changeset} ->
        Logger.error("Failed to update block field: #{inspect(changeset)}")
        {:noreply, socket}
    end
  end

  defp save_block_basic_field(socket, :block_add_field, form_params) do
    field_type = form_params["field_type"] |> String.to_existing_atom()
    field_order = form_params["field_order"] |> String.to_integer()

    form_accessor =
      case field_type do
        :basic -> "basic_field_id"
        :calc -> "calc_field_id"
      end

    Viewer.add_block_field(
      socket.assigns.block,
      field_type,
      form_params[form_accessor],
      field_order
    )
    |> case do
      :ok ->
        {:noreply,
         socket
         |> put_flash(:info, "Block field created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, changeset} ->
        Logger.error("Error adding new block field: #{inspect(changeset)}")
        {:noreply, socket}
    end
  end

  defp field_id(%BlockBasicField{basic_field: %{id: id}}), do: id
  defp field_id(%BlockCalcField{calc_field: %{id: id}}), do: id
end
