defmodule AnubisWeb.ViewLive.Index do
  @moduledoc """
  Live view listing all available views.
  """

  use AnubisWeb, :live_view

  alias Anubis.Viewer
  alias Anubis.Viewer.View

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :views, list_views())}
  end

  @impl Phoenix.LiveView
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit View")
    |> assign(:view, Viewer.get_view!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New View")
    |> assign(:view, %View{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Views")
    |> assign(:view, nil)
  end

  @impl Phoenix.LiveView
  def handle_event("delete", %{"id" => id}, socket) do
    view = Viewer.get_view!(id)
    {:ok, _} = Viewer.delete_view(view)

    {:noreply, assign(socket, :views, list_views())}
  end

  defp list_views do
    Viewer.list_views()
  end
end
