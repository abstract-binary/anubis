defmodule AnubisWeb.ViewLive.Show do
  @moduledoc """
  Live view showing a single view, including the blocks and controls
  for editing the blocks.
  """

  use AnubisWeb, :live_view

  alias Anubis.Viewer
  alias Anubis.Viewer.{Block, BlockBasicField, BlockCalcField}

  require Logger

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl Phoenix.LiveView
  def handle_params(%{"id" => id} = params, _, socket) do
    {:noreply,
     socket
     |> assign(:view, Viewer.get_view!(id))
     |> apply_action(socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :show, _params) do
    socket
    |> assign(:page_title, "Showing view")
  end

  defp apply_action(socket, :edit, _params) do
    socket
    |> assign(:page_title, "Editing view")
  end

  defp apply_action(socket, :new_block, _params) do
    socket
    |> assign(:page_title, "New Block")
    |> assign(:block, %Block{})
  end

  defp apply_action(socket, :block_add_field, %{"block_id" => block_id}) do
    socket
    |> assign(:page_title, "Adding field to block")
    |> assign(:block, find_block(block_id, socket))
    |> assign(:field, nil)
  end

  defp apply_action(socket, :block_edit_field, %{"block_id" => block_id, "field_id" => field_id}) do
    block = find_block(block_id, socket)

    socket
    |> assign(:page_title, "Editing field")
    |> assign(:block, block)
    |> assign(:field, Block.get_field!(block, field_id))
  end

  defp apply_action(socket, :edit_block, %{"block_id" => block_id}) do
    socket
    |> assign(:page_title, "Edit Text Block")
    |> assign(:block_id, block_id)
  end

  @impl Phoenix.LiveView
  def handle_event("delete-block", %{"block-id" => block_id}, socket) do
    blocks = Enum.reject(socket.assigns.view.blocks, &(&1.id == block_id))
    {:ok, view} = Viewer.update_view_blocks(socket.assigns.view, blocks)

    {:noreply, assign(socket, :view, view)}
  end

  @impl Phoenix.LiveView
  def handle_event("delete-field", %{"block-id" => block_id, "field-id" => field_id}, socket) do
    case Enum.find(socket.assigns.view.blocks, &(&1.id == block_id)) do
      nil ->
        Logger.error("Could not find block #{block_id} to remove field #{field_id} from")
        {:noreply, socket}

      block ->
        basic_field? = Enum.find(block.basic_fields, &(field_id(&1) == field_id))
        field_kind = if basic_field?, do: :basic, else: :calc

        Viewer.delete_block_field(block, field_kind, field_id)
        |> case do
          {:error, reason} ->
            {:noreply,
             socket
             |> assign(:view, Viewer.get_view!(socket.assigns.view.id))
             |> put_flash(
               :error,
               "Error removing field #{field_id} from block #{block}: #{reason}"
             )}

          :ok ->
            {:noreply, assign(socket, :view, Viewer.get_view!(socket.assigns.view.id))}
        end
    end
  end

  defp find_block(block_id, socket) do
    Enum.find(socket.assigns.view.blocks, &(&1.id == block_id))
  end

  defp field_name(%BlockBasicField{basic_field: %{name: name}}), do: name
  defp field_name(%BlockCalcField{calc_field: %{name: name}}), do: name

  defp field_id(%BlockBasicField{basic_field: %{id: id}}), do: id
  defp field_id(%BlockCalcField{calc_field: %{id: id}}), do: id
end
