defmodule AnubisWeb.ViewLive.FormComponent do
  @moduledoc """
  Live form for editing the metadata of a view (not the blocks).
  """

  use AnubisWeb, :live_component

  alias Anubis.Viewer

  @impl Phoenix.LiveComponent
  def update(%{view: view} = assigns, socket) do
    changeset = Viewer.change_view(view)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl Phoenix.LiveComponent
  def handle_event("validate", %{"view" => view_params}, socket) do
    changeset =
      socket.assigns.view
      |> Viewer.change_view(view_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"view" => view_params}, socket) do
    save_view(socket, socket.assigns.action, view_params)
  end

  defp save_view(socket, :edit, view_params) do
    case Viewer.update_view(socket.assigns.view, view_params) do
      {:ok, _view} ->
        {:noreply,
         socket
         |> put_flash(:info, "View updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_view(socket, :new, view_params) do
    case Viewer.create_view(view_params) do
      {:ok, _view} ->
        {:noreply,
         socket
         |> put_flash(:info, "View created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
