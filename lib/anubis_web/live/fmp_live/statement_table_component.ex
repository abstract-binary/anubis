defmodule AnubisWeb.FmpLive.StatementTableComponent do
  @moduledoc """
  Table showing a financial statement.
  """

  use AnubisWeb, :live_component

  alias Anubis.FMP
  alias Anubis.Synth
  alias Anubis.Synth.BasicField
  alias AnubisWeb.TableFormat

  require Logger

  @impl Phoenix.LiveComponent
  def update(%{ticker: ticker, statement: statement, years: years} = assigns, socket) do
    today = Date.utc_today()
    date_threshold = Date.new!(today.year - years, today.month, today.day)

    {mod, stmts} = FMP.get_statements(ticker, statement, after: date_threshold)
    raw_table = mod.__schema__(:source)

    fields =
      Synth.list_basic_fields()
      |> Enum.filter(fn %BasicField{} = field ->
        raw_table == field.table
      end)
      |> Enum.map(&{&1.accessor, &1})
      |> Map.new()

    table =
      to_table(stmts)
      |> Map.put(:fields, fields)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:raw_table, raw_table)
     |> assign(:stmt, table)}
  end

  @spec to_table([map()]) :: map()
  defp to_table(stmts) do
    dates = Enum.map(stmts, & &1.data["date"]) |> Enum.sort()
    headers = Enum.flat_map(stmts, &Map.keys(&1.data)) |> Enum.uniq()

    data =
      Enum.map(stmts, fn stmt ->
        data =
          Enum.map(headers, &{&1, TableFormat.prettify_cell(Map.get(stmt.data, &1), &1)})
          |> Map.new()

        {stmt.data["date"], data}
      end)
      |> Map.new()

    %{headers: headers, dates: dates, data: data}
  end

  @spec field_table_cell(
          String.t(),
          String.t(),
          String.t(),
          nil | %BasicField{},
          Phoenix.LiveView.Socket.t()
        ) ::
          Phoenix.HTML.safe()
  def field_table_cell(ticker, raw_table, field_name, nil, socket) do
    ~e[<%= live_redirect "Add", to: Routes.fmp_show_path(socket, :add_field, ticker, raw_table, field_name), class: "discreteButton"%>]
  end

  def field_table_cell(_ticker, _raw_table, _field_name, %BasicField{id: id, name: name}, socket) do
    ~e"<code><%= live_redirect name, to: Routes.field_show_path(socket, :show, id) %></code>"
  end
end
