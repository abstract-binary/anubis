defmodule AnubisWeb.FmpLive.Show do
  @moduledoc false

  use AnubisWeb, :live_view

  alias Anubis.FMP
  alias Anubis.Synth.BasicField

  @sheets [
    {"Balance Sheet", "balance-sheet-statement", :balance_sheet_statement},
    {"Cash Flow Statement", "cash-flow-statement", :cash_flow_statement},
    {"Income Statement", "income-statement", :income_statement}
  ]

  @impl Phoenix.LiveView
  def handle_params(%{"id" => id} = params, _, socket) do
    add_field_assigns =
      Enum.reduce(params, %{}, fn {key, value}, acc ->
        case key do
          "raw_table" -> Map.put(acc, :raw_table, value)
          "field_name" -> Map.put(acc, :field_name, value)
          _ -> acc
        end
      end)

    {:noreply,
     socket
     |> assign(add_field_assigns)
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:ticker, FMP.get_ticker!(id))
     |> assign(:sheets, @sheets)
     |> assign(:years, 10)}
  end

  @impl Phoenix.LiveView
  def handle_event("fmp_update", %{"ticker" => ticker}, socket) do
    case Anubis.FMP.download_all_statements(ticker) do
      :ok ->
        Enum.each(
          @sheets,
          fn {_, id, statement} ->
            send_update(AnubisWeb.FmpLive.StatementTableComponent,
              id: id,
              ticker: ticker,
              statement: statement
            )
          end
        )

        {:noreply, socket}

      {:error, reason} ->
        {:noreply, put_flash(socket, :error, "Error updating #{ticker}: #{inspect(reason)}")}
    end
  end

  @impl Phoenix.LiveView
  def handle_event("view-changed", %{"view_form" => %{"years" => years}}, socket) do
    {:noreply, assign(socket, :years, String.to_integer(years))}
  end

  defp page_title(:show), do: "Ticker Details"
  defp page_title(:add_field), do: "Adding Field"
end
