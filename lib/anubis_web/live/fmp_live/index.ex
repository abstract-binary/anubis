defmodule AnubisWeb.FmpLive.Index do
  @moduledoc """
  Main page to deal with raw FMP data.
  """

  use AnubisWeb, :live_view

  alias Anubis.FMP

  require Logger

  @max_results 40

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    filter = ""

    socket =
      socket
      |> assign(:tickers, list_tickers(filter))
      |> assign(:filter, filter)

    {:ok, socket}
  end

  @impl Phoenix.LiveView
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:max_results, @max_results)
    |> assign(:page_title, "FMP Data")
    |> assign(:ticker, nil)
  end

  @impl Phoenix.LiveView
  def handle_event("search", %{"search" => %{"filter" => filter}}, socket) do
    socket =
      socket
      |> assign(:filter, filter)
      |> assign(:tickers, list_tickers(filter))

    {:noreply, socket}
  end

  @impl Phoenix.LiveView
  def handle_event(ev, args, socket) do
    Logger.info("FMP unhandled event #{ev}: #{inspect(args)}")
    {:noreply, socket}
  end

  defp list_tickers(filter) do
    FMP.fuzzy_find_all_tickers(filter, limit: @max_results)
  end
end
