defmodule AnubisWeb.PageLive do
  @moduledoc """
  Homepage which should show a quick summary of everything.
  """

  use AnubisWeb, :live_view

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, socket}
  end
end
