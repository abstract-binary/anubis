defmodule AnubisWeb.LiveHelpers do
  @moduledoc false

  import Phoenix.LiveView.Helpers

  @doc """
  Renders a component inside the `AnubisWeb.ModalComponent` component.

  The rendered modal receives a `:return_to` option to properly update
  the URL when the modal is closed.

  ## Examples

      <%= live_modal @socket, AnubisWeb.InstrumentLive.FormComponent,
        id: @instrument.id || :new,
        action: @live_action,
        instrument: @instrument,
        return_to: Routes.instrument_index_path(@socket, :index) %>
  """
  @spec live_modal(Phoenix.LiveView.Socket.t(), atom(), Keyword.t()) ::
          Phoenix.LiveView.Component.t()
  def live_modal(socket, component, opts) do
    path = Keyword.fetch!(opts, :return_to)
    modal_opts = [id: :modal, return_to: path, component: component, opts: opts]
    live_component(socket, AnubisWeb.ModalComponent, modal_opts)
  end
end
