defmodule AnubisWeb.InstrumentLive.FormComponent do
  @moduledoc """
  Form for editing instruments.
  """

  use AnubisWeb, :live_component

  alias Anubis.Viewer

  require Logger

  @impl Phoenix.LiveComponent
  def preload(list_of_assigns) do
    Logger.info("Preloading instrument form: #{inspect(list_of_assigns)}")
    tickers = Anubis.FMP.list_statement_tickers()

    for assign <- list_of_assigns do
      Map.put(assign, :fmp_tickers, tickers)
    end
  end

  @impl Phoenix.LiveComponent
  def update(%{instrument: instrument} = assigns, socket) do
    changeset = Viewer.change_instrument(instrument)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl Phoenix.LiveComponent
  def handle_event("validate", %{"instrument" => instrument_params}, socket) do
    Logger.info("Validating #{inspect(instrument_params)}")

    changeset =
      socket.assigns.instrument
      |> Viewer.change_instrument(instrument_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"instrument" => instrument_params}, socket) do
    Logger.info("Saving #{inspect(instrument_params)}")

    save_instrument(
      socket,
      socket.assigns.action,
      Map.put(
        instrument_params,
        "fmp_updated",
        NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
      )
    )
  end

  defp save_instrument(socket, :edit, instrument_params) do
    case Viewer.update_instrument(socket.assigns.instrument, instrument_params) do
      {:ok, _instrument} ->
        {:noreply,
         socket
         |> put_flash(:info, "Instrument updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_instrument(socket, :new, instrument_params) do
    case Viewer.create_instrument(instrument_params) do
      {:ok, _instrument} ->
        {:noreply,
         socket
         |> put_flash(:info, "Instrument created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        Logger.error(
          "Failed to create #{inspect(instrument_params)}: #{inspect(changeset.errors)}"
        )

        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
