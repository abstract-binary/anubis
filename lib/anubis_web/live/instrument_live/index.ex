defmodule AnubisWeb.InstrumentLive.Index do
  @moduledoc """
  Main page to deal with instruments.
  """

  use AnubisWeb, :live_view

  alias Anubis.Viewer
  alias Anubis.Viewer.Instrument

  require Logger

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :instruments, list_instruments())}
  end

  @impl Phoenix.LiveView
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Instrument")
    |> assign(:instrument, Viewer.get_instrument!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Instrument")
    |> assign(:instrument, %Instrument{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Instruments")
    |> assign(:instrument, nil)
  end

  @impl Phoenix.LiveView
  def handle_event("delete", %{"id" => id}, socket) do
    instrument = Viewer.get_instrument!(id)
    {:ok, _} = Viewer.delete_instrument(instrument)

    {:noreply, assign(socket, :instruments, list_instruments())}
  end

  defp list_instruments do
    Viewer.list_instruments()
  end
end
