defmodule AnubisWeb.InstrumentLive.Show do
  @moduledoc false

  use AnubisWeb, :live_view

  alias Anubis.Formula
  alias Anubis.Formula.Context
  alias Anubis.Synth.{BasicField, CalcField}
  alias Anubis.Viewer
  alias Anubis.Viewer.{BlockBasicField, BlockCalcField}
  alias Anubis.Viewer.Instrument
  alias AnubisWeb.TableFormat

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl Phoenix.LiveView
  def handle_params(%{"id" => id}, _, socket) do
    views = Viewer.list_views()

    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:instrument, Viewer.get_instrument!(id))
     |> assign(:views, views)
     |> assign(:selected_view, hd(views))
     |> assign(:years, 10)}
  end

  @impl Phoenix.LiveView
  def handle_event("delete", %{"id" => id}, socket) do
    instrument = Viewer.get_instrument!(id)
    {:ok, _} = Viewer.delete_instrument(instrument)

    {:noreply, push_redirect(socket, to: Routes.instrument_index_path(socket, :index))}
  end

  @impl Phoenix.LiveView
  def handle_event("fmp_update", %{"id" => id}, socket) do
    instrument = Viewer.get_instrument!(id)

    case Anubis.FMP.download_all_statements(instrument.fmp_ticker) do
      :ok ->
        {:ok, instrument} =
          instrument
          |> Anubis.Viewer.update_instrument(%{
            fmp_updated: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
          })

        {:noreply, assign(socket, :instrument, instrument)}

      {:error, reason} ->
        {:noreply,
         put_flash(socket, :error, "Error updating #{instrument.symbol}: #{inspect(reason)}")}
    end
  end

  @impl Phoenix.LiveView
  def handle_event("view-changed", %{"view_form" => %{"view" => view, "years" => years}}, socket) do
    selected_view = Enum.find(socket.assigns.views, &(&1.id == view))

    {:noreply,
     socket
     |> assign(:selected_view, selected_view)
     |> assign(:years, String.to_integer(years))}
  end

  @spec render_fields(
          binary,
          [%BasicField{} | %CalcField{}],
          %Instrument{},
          integer
        ) ::
          Phoenix.HTML.safe()
  defp render_fields(block_id, flds, instrument, years) do
    # If you delete this line, indentation stops working
    {} = {}
    this_year = Date.utc_today().year
    years = (this_year - years)..(this_year - 1) |> Enum.to_list()

    ~e"""
    <table>
      <thead>
        <tr>
          <th></th>
          <%= for y <- years do %>
            <th><%= y %></th>
          <% end %>
        </tr>
      </thead>
      <tbody>
        <%= for fld <- flds |> Enum.sort_by(&{&1.order, extract_field(&1).name}) do %>
          <tr>
            <th class="cellWithHoverBox">
              <%= extract_field(fld).name %>
              <div class="cellHoverBox">
                <%= render_long_text(extract_field(fld).description) %>
              </div>
            </th>
            <%= render_field(block_id, extract_field(fld), years, instrument) %>
          </tr>
        <% end %>
      </tbody>
    </table>
    """
  end

  @spec render_field(
          binary,
          %BasicField{} | %CalcField{},
          [integer],
          %Instrument{}
        ) ::
          Phoenix.HTML.safe()
  defp render_field(block_id, %BasicField{} = fld, years, instrument) do
    # If you delete this line, indentation stops working
    {} = {}
    formula = Formula.new_field(fld.id)
    ctx = Context.new(years: years, symbol: instrument.fmp_ticker)
    render_formula(block_id, fld.name, formula, ctx)
  end

  defp render_field(block_id, %CalcField{} = fld, years, instrument) do
    ctx = Context.new(years: years, symbol: instrument.fmp_ticker)
    render_formula(block_id, fld.name, fld.formula, ctx)
  end

  defp render_formula(block_id, field_name, %Formula{} = formula, %Context{} = ctx) do
    case Formula.compute_for_years(formula, ctx) do
      {:ok, res} ->
        ~e"""
        <%= for y <- ctx.years do %>
          <td class="cellWithHoverBox">
            <%= TableFormat.prettify_cell(Map.get(res.values, y), field_name) %>
            <div class="cellHoverBox" phx-hook="MathFormula" id="mathFormula-<%= block_id %>-<%= field_name %>-<%= y %>">
              <%= Map.get(res.working_symbolic, y) %>
              =
              <%= Map.get(res.working_numeric, y) %>
            </div>
          </td>
        <% end %>
        """

      {:error, reason} ->
        ~e[<td><span class="errorText"><%= reason %></span></td>]
    end
  end

  defp render_long_text(str) when is_binary(str) do
    parts =
      str
      |> String.trim()
      |> String.split("\n\n")

    ~e"""
      <%= for p <- parts do %>
        <p><%= p %></p>
      <% end %>
    """
  end

  defp page_title(:show), do: "Show Instrument"
  defp page_title(:edit), do: "Edit Instrument"

  defp extract_field(%BlockBasicField{basic_field: field}), do: field
  defp extract_field(%BlockCalcField{calc_field: field}), do: field
end
