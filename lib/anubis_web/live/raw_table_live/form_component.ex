defmodule AnubisWeb.RawTableLive.FormComponent do
  @moduledoc """
  Form for editing RawTables.
  """

  use AnubisWeb, :live_component

  alias Anubis.Synth

  @impl Phoenix.LiveComponent
  def update(%{raw_table: raw_table} = assigns, socket) do
    changeset = Synth.change_raw_table(raw_table)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl Phoenix.LiveComponent
  def handle_event("validate", %{"raw_table" => raw_table_params}, socket) do
    changeset =
      socket.assigns.raw_table
      |> Synth.change_raw_table(raw_table_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"raw_table" => raw_table_params}, socket) do
    save_raw_table(socket, socket.assigns.action, raw_table_params)
  end

  defp save_raw_table(socket, :edit, raw_table_params) do
    case Synth.update_raw_table(socket.assigns.raw_table, raw_table_params) do
      {:ok, _raw_table} ->
        {:noreply,
         socket
         |> put_flash(:info, "Raw table updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_raw_table(socket, :new, raw_table_params) do
    case Synth.create_raw_table(raw_table_params) do
      {:ok, _raw_table} ->
        {:noreply,
         socket
         |> put_flash(:info, "Raw table created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
