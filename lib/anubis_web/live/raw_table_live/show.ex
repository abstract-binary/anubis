defmodule AnubisWeb.RawTableLive.Show do
  @moduledoc """
  Show a single RawTable.
  """

  use AnubisWeb, :live_view

  alias Anubis.Synth

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl Phoenix.LiveView
  def handle_params(%{"id" => table}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:raw_table, Synth.get_raw_table!(table))}
  end

  @impl Phoenix.LiveView
  def handle_event("delete", %{"id" => table}, socket) do
    raw_table = Synth.get_raw_table!(table)
    {:ok, _} = Synth.delete_raw_table(raw_table)

    {:noreply, push_redirect(socket, to: Routes.raw_table_index_path(socket, :index))}
  end

  defp page_title(:show), do: "Show Raw table"
  defp page_title(:edit), do: "Edit Raw table"
end
