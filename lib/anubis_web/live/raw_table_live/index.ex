defmodule AnubisWeb.RawTableLive.Index do
  @moduledoc """
  Page which lists RawTables.
  """

  use AnubisWeb, :live_view

  alias Anubis.Synth
  alias Anubis.Synth.RawTable

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :raw_tables, list_raw_tables())}
  end

  @impl Phoenix.LiveView
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => table}) do
    socket
    |> assign(:page_title, "Edit Raw table")
    |> assign(:raw_table, Synth.get_raw_table!(table))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Raw table")
    |> assign(:raw_table, %RawTable{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Raw tables")
    |> assign(:raw_table, nil)
  end

  @impl Phoenix.LiveView
  def handle_event("delete", %{"id" => table}, socket) do
    raw_table = Synth.get_raw_table!(table)
    {:ok, _} = Synth.delete_raw_table(raw_table)

    {:noreply, assign(socket, :raw_tables, list_raw_tables())}
  end

  defp list_raw_tables do
    Synth.list_raw_tables()
  end
end
