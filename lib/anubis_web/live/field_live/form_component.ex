defmodule AnubisWeb.FieldLive.FormComponent do
  @moduledoc """
  Form to edit fields of all kinds..
  """

  use AnubisWeb, :live_component

  alias Anubis.Synth
  alias Anubis.Synth.{BasicField, CalcField}

  @impl Phoenix.LiveComponent
  def update(%{field: field} = assigns, socket) do
    changeset =
      case field do
        %BasicField{} -> Synth.change_basic_field(field)
        %CalcField{} -> Synth.change_calc_field(field)
      end

    raw_tables = Synth.list_raw_tables() |> Enum.map(& &1.table)
    field_kinds = Synth.list_field_kinds() |> Enum.map(& &1.kind)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)
     |> assign(:raw_tables, raw_tables)
     |> assign(:field_kinds, field_kinds)}
  end

  @impl Phoenix.LiveComponent
  def handle_event("validate", %{"basic_field" => field_params}, socket) do
    changeset =
      socket.assigns.field
      |> Synth.change_basic_field(field_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("validate", %{"calc_field" => field_params}, socket) do
    changeset =
      socket.assigns.field
      |> Synth.change_calc_field(field_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"basic_field" => field_params}, socket) do
    save_field(
      socket,
      socket.assigns.action,
      :create_basic_field,
      :update_basic_field,
      field_params
    )
  end

  def handle_event("save", %{"calc_field" => field_params}, socket) do
    save_field(
      socket,
      socket.assigns.action,
      :create_basic_field,
      :update_calc_field,
      field_params
    )
  end

  defp save_field(socket, :edit, _create_f, update_f, field_params) do
    case apply(Synth, update_f, [socket.assigns.field, field_params]) do
      {:ok, _field} ->
        {:noreply,
         socket
         |> put_flash(:info, "Field updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_field(socket, :new, create_f, _update_f, field_params) do
    case apply(Synth, create_f, [field_params]) do
      {:ok, _field} ->
        {:noreply,
         socket
         |> put_flash(:info, "Field created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  defp formula_input(form, field) do
    content_tag :div do
      textarea(form, field)
    end
  end
end
