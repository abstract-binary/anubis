defmodule AnubisWeb.FieldLive.Index do
  @moduledoc """
  Page which lists all BasicFields.
  """

  use AnubisWeb, :live_view

  alias Anubis.Synth
  alias Anubis.Synth.{BasicField, CalcField}

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, refresh_fields(socket)}
  end

  @impl Phoenix.LiveView
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit field")
    |> assign(:field, Synth.get_any_field!(id))
  end

  defp apply_action(socket, :new_basic, _params) do
    socket
    |> assign(:page_title, "New Basic field")
    |> assign(:field, %BasicField{})
  end

  defp apply_action(socket, :new_calc, _params) do
    socket
    |> assign(:page_title, "New Calc field")
    |> assign(:field, %CalcField{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Fields")
    |> assign(:field, nil)
  end

  @impl Phoenix.LiveView
  def handle_event("delete", %{"id" => id}, socket) do
    case Synth.get_basic_field(id) do
      {:ok, %BasicField{} = field} ->
        {:ok, _} = Synth.delete_basic_field(field)
        :ok

      {:error, _} ->
        {:ok, _} = Synth.delete_calc_field(Synth.get_calc_field!(id))
        :ok
    end

    {:noreply, refresh_fields(socket)}
  end

  defp refresh_fields(socket) do
    fields =
      [Synth.list_basic_fields(), Synth.list_calc_fields()]
      |> Enum.concat()
      |> Enum.sort_by(& &1.name)

    assign(socket, :fields, fields)
  end
end
