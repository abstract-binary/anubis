defmodule AnubisWeb.FieldLive.Show do
  @moduledoc """
  Page which displays a single BasicField.
  """

  use AnubisWeb, :live_view

  alias Anubis.Synth
  alias Anubis.Synth.{BasicField, CalcField}

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl Phoenix.LiveView
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:field, Synth.get_any_field!(id))}
  end

  @impl Phoenix.LiveView
  def handle_event("delete", %{"id" => id}, socket) do
    basic_field = Synth.get_basic_field!(id)
    {:ok, _} = Synth.delete_basic_field(basic_field)

    {:noreply, push_redirect(socket, to: Routes.field_index_path(socket, :index))}
  end

  defp page_title(:show), do: "Show Basic field"
  defp page_title(:edit), do: "Edit Basic field"
end
