defmodule AnubisWeb.StatsLive.Show do
  @moduledoc """
  Main page to show a single series.
  """

  use AnubisWeb, :live_view

  alias Anubis.Stats

  require Logger

  @periods [
    {"yearly", :year},
    {"monthly", :month}
  ]

  @impl Phoenix.LiveView
  def handle_params(%{"id" => id}, _, socket) do
    ten_years_ago = Date.utc_today().year - 10

    {:noreply,
     socket
     |> assign(:series, id)
     |> assign(:periods, @periods)
     |> assign(:page_title, "#{id} Series")
     |> assign(:summary, Stats.summarize_series(series: id) |> Map.get(id))
     |> assign(:starting_year, ten_years_ago)}
  end

  @impl Phoenix.LiveView
  def handle_event("stats_update", %{"series" => series}, socket) do
    case Anubis.Stats.download_series(series) do
      :ok ->
        Enum.each(
          @periods,
          fn {id, period} ->
            send_update(AnubisWeb.FmpLive.StatementTableComponent,
              id: id,
              series: series,
              period: period,
              starting_year: socket.assigns.starting_year
            )
          end
        )

        {:noreply, socket}

      {:error, reason} ->
        {:noreply, put_flash(socket, :error, "Error updating #{series}: #{inspect(reason)}")}
    end
  end
end
