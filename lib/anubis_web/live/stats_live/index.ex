defmodule AnubisWeb.StatsLive.Index do
  @moduledoc """
  Main page to list downloaded stats.
  """

  use AnubisWeb, :live_view

  alias Anubis.Stats

  require Logger

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :series, Stats.summarize_series())}
  end

  @impl Phoenix.LiveView
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Stats")
  end
end
