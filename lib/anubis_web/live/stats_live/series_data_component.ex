defmodule AnubisWeb.StatsLive.SeriesDataComponent do
  @moduledoc """
  Table showing series data.
  """

  use AnubisWeb, :live_component

  alias Anubis.Stats
  alias Anubis.Stats.Misc

  require Logger

  @impl Phoenix.LiveComponent
  def update(%{series: series, period: period, starting_year: starting_year} = assigns, socket) do
    socket =
      socket
      |> assign(assigns)
      |> assign(:entries, Stats.list_series(series, period: period, starting_year: starting_year))

    {:ok, assign(socket, assigns)}
  end

  @spec cell_pct_change(%Misc{}, %Misc{}) :: Phoenix.HTML.safe()
  defp cell_pct_change(%Misc{data: %{"value" => prev}}, %Misc{data: %{"value" => cur}}) do
    prev = String.to_float(prev)
    cur = String.to_float(cur)
    change_pct = (cur - prev) / prev * 100.0

    ~e"<span class='cellGenNumber'><%= :erlang.float_to_binary(change_pct, decimals: 1) %>%</span>"
  end

  @spec normalize_series([%Misc{}]) :: [float()]
  defp normalize_series(entries) do
    Enum.map(entries, &String.to_float(&1.data["value"]))
    |> case do
      [] ->
        []

      [first | rest] ->
        [100.0 | Enum.map(rest, &(&1 * 100.0 / first))]
    end
  end

  defmodule RC do
    @moduledoc """
    Root calculator: calculates roots to the nth degree.
    """

    @spec nth_root(float(), integer(), float()) :: float()
    def nth_root(x, n, precision \\ 1.0e-5) do
      f = fn prev -> ((n - 1) * prev + x / :math.pow(prev, n - 1)) / n end
      fixed_point(f, x, precision, f.(x))
    end

    defp fixed_point(_, guess, tolerance, next) when abs(guess - next) < tolerance, do: next
    defp fixed_point(f, _, tolerance, next), do: fixed_point(f, next, tolerance, f.(next))
  end

  # Computes the Compound Growth Rate of the series.  This is the rate
  # at which the values would have had to increase compunded from
  # start to end to match the series start and end.
  @spec compound_growth_rate([%Misc{}]) :: Phoenix.HTML.safe()
  defp compound_growth_rate(entries) do
    cgr =
      case {List.first(entries), List.last(entries)} do
        {%Misc{data: %{"value" => first}}, %Misc{data: %{"value" => last}}} ->
          first = String.to_float(first)
          last = String.to_float(last)
          RC.nth_root(last / first, length(entries) - 1) - 1

        {_, _} ->
          0.0
      end

    ~e"<span class='cellGenNumber'><%= :erlang.float_to_binary(100 * cgr, decimals: 2) %>%</span>"
  end
end
