defmodule AnubisWeb.TableFormat do
  @moduledoc """
  Helpers for formatting tables in EEx.
  """

  use AnubisWeb, :helper_module

  @spec short_date(String.t()) :: String.t()
  def short_date(str) do
    case Date.from_iso8601(str) do
      {:error, _} ->
        str

      {:ok, date} ->
        case {date.month, date.day} do
          {12, 31} -> "#{date.year}"
          {9, 30} -> "#{date.year}Q3"
          {6, 30} -> "#{date.year}Q2"
          {3, 31} -> "#{date.year}Q1"
          _ -> str
        end
    end
  end

  @spec prettify_cell(term(), String.t()) :: Phoenix.HTML.safe()
  def prettify_cell(v, header) do
    case v do
      0 ->
        ~e"<span class='cellZero'>-</span>"

      0.0 ->
        ~e"<span class='cellZero'>-</span>"

      # n when is_number(n) and abs(n) >= 10_000_000 ->
      #   ~e"<span class='cellBNumber'><%= :erlang.float_to_binary(n / 1_000_000_000, decimals: 2) %></span>"

      n when is_number(n) and abs(n) >= 10_000 ->
        s =
          (n / 1_000_000.0)
          |> Number.Delimit.number_to_delimited(precision: 0)

        ~e"<span class='cellMNumber'><%= s %></span>"

      n when is_number(n) ->
        case header do
          "roe" ->
            ~e"<span class='cellGenNumber'><%= :erlang.float_to_binary(n * 100, decimals: 2) %>%</span>"

          _ ->
            ~e"<span class='cellGenNumber'><%= :erlang.float_to_binary(n / 1.0, decimals: 2) %></span>"
        end

      <<"https://", _::binary>> ->
        ~e"<span class='cellLink'><a href='<%= v %>'>Link</a></span>"

      _ ->
        ~e"<span class='cellText'><%= v %></span>"
    end
  end
end
