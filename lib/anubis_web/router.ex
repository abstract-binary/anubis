defmodule AnubisWeb.Router do
  use AnubisWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {AnubisWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/api", AnubisWeb do
    pipe_through :api
  end

  scope "/", AnubisWeb do
    pipe_through :browser

    live "/", PageLive, :index
  end

  scope "/instruments", AnubisWeb do
    pipe_through :browser

    live "/", InstrumentLive.Index, :index
    live "/new", InstrumentLive.Index, :new
    live "/:id/edit", InstrumentLive.Index, :edit

    live "/:id", InstrumentLive.Show, :show
    live "/:id/show/edit", InstrumentLive.Show, :edit
  end

  scope "/fmp", AnubisWeb do
    pipe_through :browser

    live "/", FmpLive.Index, :index
    live "/:id", FmpLive.Show, :show
    live "/:id/add_field/:raw_table/:field_name", FmpLive.Show, :add_field
  end

  scope "/raw_tables", AnubisWeb do
    pipe_through :browser

    live "/", RawTableLive.Index, :index
    live "/new", RawTableLive.Index, :new
    live "/:id/edit", RawTableLive.Index, :edit

    live "/:id", RawTableLive.Show, :show
    live "/:id/show/edit", RawTableLive.Show, :edit
  end

  scope "/fields", AnubisWeb do
    pipe_through :browser

    live "/", FieldLive.Index, :index
    live "/new_basic", FieldLive.Index, :new_basic
    live "/new_calc", FieldLive.Index, :new_calc
    live "/:id/edit", FieldLive.Index, :edit

    live "/:id", FieldLive.Show, :show
    live "/:id/show/edit", FieldLive.Show, :edit
  end

  scope "/stats", AnubisWeb do
    pipe_through :browser

    live "/", StatsLive.Index, :index

    live "/:id", StatsLive.Show, :show
  end

  scope "/views", AnubisWeb do
    pipe_through :browser

    live "/", ViewLive.Index, :index
    live "/new", ViewLive.Index, :new
    live "/:id/edit", ViewLive.Index, :edit

    live "/:id", ViewLive.Show, :show
    live "/:id/show/edit", ViewLive.Show, :edit
    live "/:id/show/new_block", ViewLive.Show, :new_block
    live "/:id/show/edit_block/:block_id", ViewLive.Show, :edit_block
    live "/:id/show/block_add_field/:block_id", ViewLive.Show, :block_add_field
    live "/:id/show/block_edit_field/:block_id/:field_id", ViewLive.Show, :block_edit_field
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: AnubisWeb.Telemetry
    end
  end
end
