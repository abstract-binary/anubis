defmodule Anubis.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @spec start(any(), any()) :: {:error, any()} | {:ok, pid()} | {:ok, pid(), any()}
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Anubis.Repo,
      # Start the Telemetry supervisor
      AnubisWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Anubis.PubSub},
      # Start the Endpoint (http/https)
      AnubisWeb.Endpoint,
      # Start a worker by calling: Anubis.Worker.start_link(arg)
      # {Anubis.Worker, arg}
      {Finch, name: MyFinch}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Anubis.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @spec config_change(term(), term(), term()) :: :ok
  def config_change(changed, _new, removed) do
    AnubisWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
