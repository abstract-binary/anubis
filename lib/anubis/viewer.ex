defmodule Anubis.Viewer do
  @moduledoc """
  The Viewer context.
  """

  import Ecto.Query, warn: false

  alias Anubis.Repo
  alias Anubis.Viewer.{Block, BlockBasicField, BlockCalcField, Instrument, View}

  require Logger

  @spec list_instruments() :: [%Instrument{}]
  def list_instruments do
    Instrument
    |> Ecto.Query.order_by([i], asc: i.symbol)
    |> Repo.all()
  end

  @spec get_instrument!(integer) :: %Instrument{}
  def get_instrument!(id), do: Repo.get!(Instrument, id)

  @spec create_instrument(map()) :: {:ok, %Instrument{}} | {:error, Ecto.Changeset.t()}
  def create_instrument(attrs \\ %{}) do
    %Instrument{}
    |> Instrument.changeset(attrs)
    |> Repo.insert()
  end

  @spec update_instrument(%Instrument{}, map()) ::
          {:ok, %Instrument{}} | {:error, Ecto.Changeset.t()}
  def update_instrument(%Instrument{} = instrument, attrs) do
    instrument
    |> Instrument.changeset(attrs)
    |> Repo.update()
  end

  @spec delete_instrument(%Instrument{}) :: {:ok, %Instrument{}} | {:error, Ecto.Changeset.t()}
  def delete_instrument(%Instrument{} = instrument) do
    Repo.delete(instrument)
  end

  @spec change_instrument(%Instrument{}, map()) :: Ecto.Changeset.t()
  def change_instrument(%Instrument{} = instrument, attrs \\ %{}) do
    Instrument.changeset(
      instrument,
      Map.put(attrs, "fmp_updated", NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second))
    )
  end

  @spec list_views() :: [%View{}]
  def list_views do
    View
    |> preload(blocks: [basic_fields: :basic_field, calc_fields: :calc_field])
    |> Repo.all()
  end

  @spec get_view!(binary) :: %View{}
  def get_view!(id) do
    View
    |> preload(blocks: [basic_fields: :basic_field, calc_fields: :calc_field])
    |> Repo.get!(id)
  end

  @spec create_view(map()) :: {:ok, %View{}} | {:error, Ecto.Changeset.t()}
  def create_view(attrs \\ %{}) do
    %View{}
    |> View.changeset(attrs)
    |> Repo.insert()
  end

  @spec update_view(%View{}, map()) :: {:ok, %View{}} | {:error, Ecto.Changeset.t()}
  def update_view(%View{} = view, attrs) do
    view
    |> View.changeset(attrs)
    |> Repo.update()
  end

  @spec delete_view(%View{}) :: {:ok, %View{}} | {:error, Ecto.Changeset.t()}
  def delete_view(%View{} = view) do
    Repo.delete(view)
  end

  @spec change_view(%View{}, map()) :: Ecto.Changeset.t()
  def change_view(%View{} = view, attrs \\ %{}) do
    View.changeset(view, attrs)
  end

  @spec update_view_blocks(%View{}, [%Block{} | Ecto.Changeset.t()]) ::
          {:ok, %View{}} | {:error, Ecto.Changeset.t()}
  def update_view_blocks(%View{} = view, blocks) do
    view
    |> change_view()
    |> Ecto.Changeset.put_assoc(:blocks, blocks)
    |> Repo.update()
  end

  @spec list_blocks() :: [%Block{}]
  def list_blocks() do
    Block
    |> preload(basic_fields: :basic_field, calc_fields: :calc_field)
    |> Repo.all()
  end

  @spec get_block!(binary) :: %Block{}
  def get_block!(id) do
    Block
    |> preload(basic_fields: :basic_field, calc_fields: :calc_field)
    |> Repo.get!(id)
  end

  @spec change_block(%Block{}, map()) :: Ecto.Changeset.t()
  def change_block(%Block{} = block, attrs \\ %{}) do
    Block.changeset(block, attrs)
  end

  @spec update_block(%Block{}, map()) :: {:ok, %Block{}} | {:error, Ecto.Changeset.t()}
  def update_block(%Block{} = block, attrs) do
    block
    |> Block.changeset(attrs)
    |> Repo.update()
  end

  @spec add_block_field(%Block{}, :basic | :calc, binary, integer) ::
          :ok | {:error, Ecto.Changeset.t()}
  def add_block_field(%Block{} = block, kind, field_id, order) do
    fields = %{
      block_id: block.id,
      order: order
    }

    case kind do
      :basic ->
        BlockBasicField.changeset(%BlockBasicField{}, Map.put(fields, :basic_field_id, field_id))

      :calc ->
        BlockCalcField.changeset(%BlockCalcField{}, Map.put(fields, :calc_field_id, field_id))
    end
    |> Repo.insert()
    |> case do
      {:ok, _} -> :ok
      {:error, changeset} -> {:error, changeset}
    end
  end

  @spec update_block_field(%Block{}, :basic | :calc, binary, integer) ::
          :ok | {:error, Ecto.Changeset.t()}
  def update_block_field(%Block{} = block, kind, field_id, order) do
    case kind do
      :basic -> BlockBasicField |> where([f], f.basic_field_id == ^field_id)
      :calc -> BlockCalcField |> where([f], f.calc_field_id == ^field_id)
    end
    |> where([f], f.block_id == ^block.id)
    |> Repo.one()
    |> case do
      nil -> add_block_field(block, kind, field_id, order)
      field -> field |> Ecto.Changeset.change(%{order: order}) |> Repo.update()
    end
    |> case do
      :ok -> :ok
      {:ok, _} -> :ok
      {:error, changeset} -> {:error, changeset}
    end
  end

  @spec delete_block_field(%Block{}, :basic | :calc, binary) :: :ok | {:error, String.t()}
  def delete_block_field(block, kind, field_id) do
    {num, nil} =
      case kind do
        :basic -> BlockBasicField |> where([f], f.basic_field_id == ^field_id)
        :calc -> BlockCalcField |> where([f], f.calc_field_id == ^field_id)
      end
      |> where([f], f.block_id == ^block.id)
      |> Repo.delete_all()

    case num do
      1 -> :ok
      _ -> {:error, "Deleted number of fields different than 1: #{num}"}
    end
  end
end
