defmodule Anubis.Formula.Result do
  @moduledoc """
  Result of a successful formula calculation.  This holds both the
  value and the working of the calculation.
  """

  alias __MODULE__

  @enforce_keys [:values, :working_symbolic, :working_numeric]
  defstruct [:values, :working_symbolic, :working_numeric]

  @type t :: %Result{
          values: map() | any(),
          working_symbolic: map() | String.t(),
          working_numeric: map() | String.t()
        }

  @spec new(map() | any(), map() | String.t(), map() | String.t()) :: Result.t()
  def new(values, working_symbolic, working_numeric) do
    %Result{values: values, working_symbolic: working_symbolic, working_numeric: working_numeric}
  end
end
