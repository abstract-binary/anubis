defmodule Anubis.Formula.QueryParams do
  @moduledoc """
  Query parameters for the field value functions.
  """

  alias __MODULE__

  @enforce_keys [:symbol, :accessor, :year]
  defstruct [:symbol, :accessor, :year]
  @type t :: %QueryParams{symbol: String.t(), accessor: String.t(), year: integer}
end
