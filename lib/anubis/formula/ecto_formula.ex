defmodule Anubis.Formula.EctoFormula do
  @moduledoc """
  Ecto type for a Formula encoded in the database.
  """

  use Ecto.Type

  alias Anubis.Formula

  @spec type() :: :string
  def type do
    :string
  end

  @spec cast(%Formula{} | String.t()) :: {:ok, String.t()} | {:error, Keyword.t()}
  def cast(%Formula{} = formula) do
    case Jason.encode(formula) do
      {:ok, str} ->
        {:ok, str}

      {:error, reason} ->
        {:error, [{:message, "Can't encode #{inspect(formula)}: #{inspect(reason)}"}]}
    end
  end

  def cast(str) when is_binary(str) do
    case load(str) do
      {:ok, x} -> cast(x)
      err -> err
    end
  end

  def cast(x) do
    {:error, [{:message, "Can't convert #{inspect(x)} to EctoFormula"}]}
  end

  @spec load(String.t()) :: {:ok, %Formula{}} | :error
  def load(str) when is_binary(str) do
    case Jason.decode(str, keys: :atoms!) do
      {:error, _} -> :error
      {:ok, map} -> {:ok, Formula.atomize_after_json_decode(map)}
    end
  end

  @spec dump(String.t()) :: {:ok, String.t()}
  def dump(str) when is_binary(str) do
    {:ok, str}
  end
end
