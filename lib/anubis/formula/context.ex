defmodule Anubis.Formula.Context do
  @moduledoc """
  Arguments used for computing the values of formulas.  The idea is
  that sub-formulas can have their own context, and when fields are
  evaluated, they use the closest values defined by one of the
  contexts above them.
  """

  alias __MODULE__

  @derive Jason.Encoder
  @enforce_keys []
  defstruct [:years, :symbol]

  @type t :: %Context{years: [integer()], symbol: String.t()}

  @spec new(Keyword.t()) :: t()
  def new(opts \\ []) do
    years = Keyword.get(opts, :years)
    symbol = Keyword.get(opts, :symbol)
    %Context{years: years, symbol: symbol}
  end

  @spec merge(t(), t()) :: t()
  def merge(t, other) do
    %Context{
      years: other.years || t.years,
      symbol: other.symbol || t.symbol
    }
  end
end
