defmodule Anubis.FMP do
  @moduledoc """
  Interface to the remote financialmodelingprep.com API and the
  `fmp_*` database tables with the downloaded data.
  """

  import Ecto.Query, warn: false

  alias Anubis.FMP.BalanceSheetStatement, as: BSS
  alias Anubis.FMP.CashFlowStatement, as: CFS
  alias Anubis.FMP.IncomeStatement, as: IS
  alias Anubis.FMP.Ticker
  alias Anubis.Formula.QueryParams
  alias Anubis.Repo

  require Logger

  @fmp_url Application.compile_env!(:anubis, :fmp_url)

  @spec query_balance_sheet_statement(QueryParams.t()) :: {:ok, float()} | {:error, String.t()}
  def query_balance_sheet_statement(qp) do
    query_statement(BSS, qp.accessor, qp.symbol, qp.year)
  end

  @spec query_cash_flow_statement(QueryParams.t()) :: {:ok, float()} | {:error, String.t()}
  def query_cash_flow_statement(qp) do
    query_statement(CFS, qp.accessor, qp.symbol, qp.year)
  end

  @spec query_income_statement(QueryParams.t()) :: {:ok, float()} | {:error, String.t()}
  def query_income_statement(qp) do
    query_statement(IS, qp.accessor, qp.symbol, qp.year)
  end

  @spec get_ticker!(String.t()) :: %Ticker{}
  def get_ticker!(ticker) do
    Repo.get!(Ticker, ticker)
  end

  @spec get_statements(String.t(), :balance_sheet_statement, Keyword.t()) :: {module(), [%BSS{}]}
  @spec get_statements(String.t(), :cash_flow_statement, Keyword.t()) :: {module(), [%CFS{}]}
  @spec get_statements(String.t(), :income_statement, Keyword.t()) :: {module(), [%IS{}]}
  def get_statements(ticker, kind, opts \\ []) do
    mod =
      case kind do
        :balance_sheet_statement -> BSS
        :cash_flow_statement -> CFS
        :income_statement -> IS
      end

    ticker_cond = dynamic([stmt], stmt.ticker == ^ticker)

    date_cond =
      case Keyword.get(opts, :after) do
        nil -> dynamic([], true)
        after_ -> dynamic([], fragment("(data->>'date')::DATE >= ?::DATE", ^after_))
      end

    conditions = dynamic([], ^ticker_cond and ^date_cond)

    {mod,
     mod
     |> Ecto.Query.where(^conditions)
     |> Repo.all()}
  end

  @spec fuzzy_find_all_tickers(String.t(), Keyword.t()) :: [%Ticker{}]
  def fuzzy_find_all_tickers(query, opts \\ []) do
    limit = Keyword.get(opts, :limit, 10)

    Ticker
    |> Ecto.Query.where(
      [_],
      fragment(
        "(ticker || ' ' || name || ' ' || exchange) ILIKE '%' || ? || '%'",
        ^query
      )
    )
    |> Ecto.Query.limit(^limit)
    |> Ecto.Query.order_by([t], asc: t.ticker)
    |> Repo.all()
  end

  @spec list_statement_tickers() :: [String.t()]
  def list_statement_tickers do
    [BSS, CFS, IS]
    |> Enum.map(fn mod ->
      Ecto.Query.from(stmt in mod, select: stmt.ticker, distinct: true)
      |> Repo.all()
    end)
    |> List.flatten()
    |> Enum.uniq()
    |> Enum.sort()
  end

  @spec download_all_tickers() :: :ok | {:error, any()}
  def download_all_tickers do
    Logger.info("Loading all tickers")

    with {:ok, resp} <-
           Finch.build(
             :get,
             "#{@fmp_url}/#{Ticker.url_path()}"
           )
           |> Finch.request(MyFinch),
         {:ok, jsons} <- Jason.decode(resp.body) do
      Logger.info("Got #{inspect(length(jsons))} tickers")

      for json <- jsons do
        ticker = json["symbol"]
        name = json["name"]
        exchange = json["exchange"]

        if ticker != "" and name != "" and exchange != "" do
          changeset =
            case Repo.get(Ticker, ticker) do
              nil -> %Ticker{ticker: ticker}
              ticker -> ticker
            end
            |> Ticker.changeset(%{
              name: name,
              exchange: exchange
            })

          {:ok, _} = Repo.insert_or_update(changeset)
        else
          Logger.error("Ticker with missing fields #{inspect(json)}")
        end
      end

      :ok
    end
  end

  @spec download_all_statements(String.t()) :: :ok | {:error, any()}
  def download_all_statements(ticker) do
    with :ok <- download_balance_sheet_statements(ticker),
         :ok <- download_income_statements(ticker),
         :ok <- download_cash_flow_statements(ticker) do
      :ok
    end
  end

  @spec download_balance_sheet_statements(String.t()) :: :ok | {:error, any()}
  defp download_balance_sheet_statements(ticker) do
    download_statements(BSS, "balance sheet statements", ticker)
  end

  @spec download_income_statements(String.t()) :: :ok | {:error, any()}
  defp download_income_statements(ticker) do
    download_statements(IS, "income statements", ticker)
  end

  @spec download_cash_flow_statements(String.t()) :: :ok | {:error, any()}
  defp download_cash_flow_statements(ticker) do
    download_statements(CFS, "cash flow statements", ticker)
  end

  @spec download_statements(module(), String.t(), String.t()) :: :ok | {:error, any()}
  defp download_statements(mod, resource, ticker) do
    Logger.info("Loading #{resource} for #{ticker}")

    with {:ok, resp} <-
           Finch.build(
             :get,
             "#{@fmp_url}/#{mod.url_path(ticker)}"
           )
           |> Finch.request(MyFinch),
         {:ok, jsons} <- Jason.decode(resp.body) do
      for json <- jsons do
        date = json["date"]
        Logger.info("Got reply for date #{date}")

        changeset = mod.changeset(struct(mod), %{ticker: ticker, data: json})

        {previous_entries, _} =
          mod
          |> Ecto.Query.where([stmt], stmt.ticker == ^ticker)
          |> Ecto.Query.where([_], fragment("data->>'date' = ?", ^date))
          |> Repo.delete_all()

        if previous_entries > 0,
          do: Logger.info("Deleted #{inspect(previous_entries)} previous entries")

        {:ok, _} = Repo.insert(changeset)
      end

      :ok
    end
  end

  @spec query_statement(module(), String.t(), String.t(), integer()) ::
          {:ok, float()} | {:error, String.t()}
  defp query_statement(mod, accessor, ticker, year) do
    mod
    |> where([stmt], stmt.ticker == ^ticker)
    |> where(
      [_],
      fragment("(EXTRACT(year FROM date (data->>'date')) :: text) = (? :: text)", ^inspect(year))
    )
    |> Repo.one()
    |> case do
      nil ->
        {:error, "No #{mod} for #{ticker} in #{year}"}

      stmt ->
        case Map.get(stmt.data, accessor) do
          nil -> {:error, "#{mod} #{ticker}/#{year} has no field #{accessor}"}
          value -> {:ok, value}
        end
    end
  end
end
