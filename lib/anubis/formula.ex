defmodule Anubis.Formula do
  @moduledoc """
  A computable formula involving constants and fields.
  """

  alias __MODULE__
  alias Anubis.Formula.Context
  alias Anubis.Formula.QueryParams
  alias Anubis.Synth

  require Logger

  @derive Jason.Encoder
  @enforce_keys [:kind]
  defstruct [:kind, :field, :calc, :context]

  @type t ::
          %Formula{kind: :field, field: binary, context: Context.t()}
          | %Formula{kind: :calc, calc: {atom(), [Formula.t()]}}

  defimpl Phoenix.HTML.Safe, for: Formula do
    @spec to_iodata(%Formula{}) :: binary()
    def to_iodata(t) do
      Jason.encode!(t)
    end
  end

  @spec new_field(binary, Keyword.t()) :: %Formula{
          kind: :field,
          field: binary,
          context: Context.t()
        }
  def new_field(field, opts \\ []) do
    %Formula{kind: :field, field: field, context: Context.new(opts)}
  end

  @spec new_calc(atom, [Formula.t()]) :: %Formula{
          kind: :calc,
          calc: {atom(), [Formula.t()]},
          context: nil
        }
  def new_calc(op, args) do
    %Formula{kind: :calc, calc: {op, args}}
  end

  @spec new_dwim(String.t()) :: %Formula{kind: :field, field: binary, context: Context.t()}
  @spec new_dwim({atom(), [term()]}) :: %Formula{kind: :calc, calc: {atom(), [Formula.t()]}}
  def new_dwim(str) when is_binary(str) do
    case Synth.get_field_by_name(str) do
      nil -> raise "Field #{str} not found"
      field -> Formula.new_field(field.id)
    end
  end

  def new_dwim({op, args}) when is_atom(op) and is_list(args) do
    Formula.new_calc(op, Enum.map(args, &Formula.new_dwim/1))
  end

  @spec compute_for_years(Formula.t(), Context.t()) ::
          {:ok, Formula.Result.t()} | {:error, String.t()}
  def compute_for_years(
        %Formula{kind: :field, field: field, context: field_ctx},
        %Context{} = formula_ctx
      ) do
    ctx = Context.merge(formula_ctx, field_ctx)

    with {:ok, field} <- Synth.get_basic_field(field),
         {:ok, raw_table} <- Synth.get_raw_table(field.table) do
      {values, errors, working_symbolic, working_numeric} =
        for year <- ctx.years, reduce: {%{}, %{}, %{}, %{}} do
          {values, errors, working_symbolic, working_numeric} ->
            apply(
              String.to_existing_atom("Elixir." <> raw_table.module),
              String.to_existing_atom(raw_table.function),
              [%QueryParams{accessor: field.accessor, symbol: ctx.symbol, year: year}]
            )
            |> case do
              {:ok, value} ->
                {Map.put(values, year, value), errors,
                 Map.put(working_symbolic, year, "#{latex_identifier(field.name)}[#{year}]"),
                 Map.put(working_numeric, year, "#{latex_number(value)}")}

              {:error, err} ->
                {values, Map.put(errors, year, err), working_symbolic, working_numeric}
            end
        end

      if Enum.empty?(errors) do
        {:ok, Formula.Result.new(values, working_symbolic, working_numeric)}
      else
        {:error,
         "Formula.compute_for_years: Error evaluating field #{inspect(field)}: #{inspect(errors)}"}
      end
    end
  end

  def compute_for_years(%Formula{kind: :calc, calc: {op, args}}, %Context{} = ctx) do
    case {op, args} do
      {:abs, [arg]} ->
        with {:ok, result} <- compute_for_years(arg, ctx) do
          Enum.reduce_while(ctx.years, {%{}, %{}, %{}}, fn y, {acc_n, acc_ws, acc_wn} ->
            case value_for_year(result.values, y) do
              n when is_number(n) ->
                {:cont,
                 {Map.put(acc_n, y, abs(n)),
                  Map.put(acc_ws, y, "abs(#{value_for_year(result.working_symbolic, y)})"),
                  Map.put(acc_wn, y, "abs(#{value_for_year(result.working_numeric, y)})")}}

              x ->
                {:halt, {:error, "Value #{inspect(x)} for year #{y} is not a number"}}
            end
          end)
          |> case do
            {res, working_symbolic, working_numeric} ->
              {:ok, Formula.Result.new(res, working_symbolic, working_numeric)}

            {:error, err} ->
              {:error, err}
          end
        end

      {:/, [arg1, arg2]} ->
        with {:ok, result1} <- compute_for_years(arg1, ctx),
             {:ok, result2} <- compute_for_years(arg2, ctx) do
          {values, working} =
            Enum.map(ctx.years, fn year ->
              {{year,
                value_for_year(result1.values, year) / value_for_year(result2.values, year)},
               {{year,
                 latex_frac(
                   value_for_year(result1.working_symbolic, year),
                   value_for_year(result2.working_symbolic, year)
                 )},
                {year,
                 latex_frac(
                   value_for_year(result1.working_numeric, year),
                   value_for_year(result2.working_numeric, year)
                 )}}}
            end)
            |> Enum.unzip()

          {working_symbolic, working_numeric} = Enum.unzip(working)

          values = Map.new(values)
          working_symbolic = Map.new(working_symbolic)
          working_numeric = Map.new(working_numeric)

          {:ok, Formula.Result.new(values, working_symbolic, working_numeric)}
        end

      {:sum, [arg]} ->
        with {:ok, result} <- compute_for_years(arg, ctx) do
          Enum.reduce_while(ctx.years, {0, [], []}, fn year, {acc_n, acc_ws, acc_wn} ->
            case value_for_year(result.values, year) do
              n when is_number(n) ->
                {:cont,
                 {acc_n + n, ["#{value_for_year(result.working_symbolic, year)}" | acc_ws],
                  ["#{value_for_year(result.working_numeric, year)}" | acc_wn]}}

              x ->
                {:halt, {:error, "Value #{inspect(x)} for year #{year} is not a number"}}
            end
          end)
          |> case do
            {total, working_symbolic, working_numeric} when is_number(total) ->
              {:ok,
               Formula.Result.new(
                 total,
                 "sum(#{Enum.join(Enum.reverse(working_symbolic), ", ")})",
                 "sum(#{Enum.join(Enum.reverse(working_numeric), ", ")})"
               )}

            {:error, err} ->
              {:error, err}
          end
        end

      _ ->
        {:error,
         "Formula.compute_for_years: Operator '#{op}' with #{length(args)} args not known"}
    end
  end

  def compute_for_years(%Formula{} = formula, %Context{}) do
    {:error, "Formula.compute_for_years: Don't know how to handle #{inspect(formula)}"}
  end

  @spec atomize_after_json_decode(term()) :: Formula.t()
  def atomize_after_json_decode(%{kind: "field", field: str, context: ctx}) do
    %Formula{kind: :field, field: str, context: ctx}
  end

  def atomize_after_json_decode(%{kind: "calc", calc: [op, args]}) do
    %Formula{
      kind: :calc,
      calc: {String.to_existing_atom(op), Enum.map(args, &atomize_after_json_decode(&1))}
    }
  end

  def atomize_after_json_decode(formula) do
    Logger.info("Fallthrough json decoding: #{inspect(formula)}")
    formula
  end

  # Takes a `values` result from `compute_for_years` and returns the
  # value for a particular year.  If the `values` is just a number or
  # a string, then that value is returned.
  @spec value_for_year(map() | any(), integer()) :: any()
  defp value_for_year(%{} = values, year) do
    case Map.get(values, year) do
      nil -> raise "Could not find value for #{year} in #{inspect(values)}"
      value -> value
    end
  end

  defp value_for_year(value, _year) when is_number(value) or is_binary(value) do
    value
  end

  @spec latex_identifier(String.t()) :: String.t()
  defp latex_identifier(str) when is_binary(str) do
    s = String.replace(str, "_", "\\_")
    "\\mathrm{#{s}}"
  end

  @spec latex_frac(any(), any()) :: String.t()
  defp latex_frac(num, den) do
    "\\dfrac{#{num}}{#{den}}"
  end

  @spec latex_number(integer() | float()) :: String.t()
  defp latex_number(n) when is_integer(n) do
    n
    |> Number.Delimit.number_to_delimited(precision: 0)
    |> String.replace(",", "\\,")
  end

  defp latex_number(x) when is_float(x) do
    x
    |> Number.Delimit.number_to_delimited(precision: 2)
    |> String.replace(",", "\\,")
  end

  defp latex_number(s) when is_binary(s) do
    s
  end
end

defmodule Anubis.TupleEncoder do
  @moduledoc """
  Needed because Jason doesn't know how to encode tuples by default.
  """

  alias Jason.Encoder

  defimpl Encoder, for: Tuple do
    @spec encode(tuple(), Jason.Encode.opts()) :: iolist()
    def encode(data, options) when is_tuple(data) do
      data
      |> Tuple.to_list()
      |> Encoder.List.encode(options)
    end
  end
end
