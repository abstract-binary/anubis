defmodule Anubis.Synth.CalcField do
  @moduledoc """
  Schema for a field with a calculation.
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias Anubis.Formula.EctoFormula

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "calc_fields" do
    field :description, :string
    field :formula, EctoFormula
    field :name, :string

    timestamps()
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(calc_field, attrs) do
    calc_field
    |> cast(attrs, [:name, :description, :formula])
    |> validate_required([:name, :description, :formula])
  end
end
