defmodule Anubis.Synth.FieldKind do
  @moduledoc """
  The kind of a field indicates how the value for it should be
  calculated (e.g. by looking it up in another table, by running an
  Elixir function, or by applying some formula).
  """

  # TODO Maybe I don't need this.  Is not like I can put all the
  # fields in one table anyway.

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:kind, :string, autogenerate: false}
  @foreign_key_type :string
  schema "field_kinds" do
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(field_kind, attrs) do
    field_kind
    |> cast(attrs, [:kind])
    |> validate_required([:kind])
  end
end
