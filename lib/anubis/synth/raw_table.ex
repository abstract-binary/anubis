defmodule Anubis.Synth.RawTable do
  @moduledoc """
  The name of a table that can be used for `basic` fields along with
  pointers to the Elixir module and function to query it with.
  """

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:table, :string, autogenerate: false}
  @foreign_key_type :string
  schema "raw_tables" do
    field :description, :string
    field :function, :string
    field :module, :string

    timestamps()
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(raw_table, attrs) do
    raw_table
    |> cast(attrs, [:table, :description, :module, :function])
    |> validate_required([:table, :description, :module, :function])
  end
end
