defmodule Anubis.Synth.BasicField do
  @moduledoc """
  The schema for a basic field which just references another table.
  """

  use Ecto.Schema
  import Ecto.Changeset

  # TODO Add "symbol" here as an optional way of fixing the symbol
  # (which will be the difference between cpi and hicp).

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "basic_fields" do
    field :accessor, :string
    field :description, :string
    field :name, :string
    field :table, :string
    field :kind, Ecto.Enum, values: [:scalar, :year_series]

    timestamps()
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(basic_field, attrs) do
    basic_field
    |> cast(attrs, [:name, :description, :accessor, :table, :kind])
    |> validate_required([:name, :description, :accessor, :table, :kind])
    |> foreign_key_constraint(:kind, name: "basic_fields_kind_fkey")
    |> foreign_key_constraint(:table, name: "basic_fields_table_fkey")
  end
end
