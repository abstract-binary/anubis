defmodule Anubis.Stats do
  @moduledoc """
  Interface to the `stats_misc` table in the database and the series
  that it contains.
  """

  import Ecto.Query, warn: false

  alias Anubis.Formula.QueryParams
  alias Anubis.Repo
  alias Anubis.Stats.Misc

  require Logger

  @cpi_series_name "CPI"
  @hicp_series_name "HICP"

  @spec query_series(QueryParams.t()) :: {:ok, float()} | {:error, String.t()}
  def query_series(qp) do
    Misc
    |> where([x], x.year == ^qp.year and x.series == ^qp.symbol)
    |> Repo.one()
    |> case do
      nil ->
        {:error, "No series entry #{qp.series} for #{qp.year}"}

      entry ->
        case Map.get(entry.data, qp.accessor) do
          nil -> {:error, "Series entry #{qp.series}/#{qp.year} has no field #{qp.accessor}"}
          value -> {:ok, String.to_float(value)}
        end
    end
  end

  @spec summarize_series(Keyword.t()) :: map()
  def summarize_series(opts \\ []) do
    series_filter =
      case Keyword.get(opts, :series) do
        nil -> dynamic(true)
        series -> dynamic([x], x.series == ^series)
      end

    {:ok, res} =
      Repo.transaction(fn ->
        Misc
        |> where(^series_filter)
        |> Repo.stream()
        |> Enum.reduce(%{}, fn entry, acc ->
          summary =
            Map.get(acc, entry.series, %{
              years: MapSet.new(),
              entries: 0,
              last_updated: ~N[2000-01-01 00:00:00]
            })
            |> update_in([:years], &MapSet.put(&1, entry.year))
            |> update_in([:entries], &(&1 + 1))
            |> update_in([:last_updated], fn last_updated ->
              case NaiveDateTime.compare(last_updated, entry.updated_at) do
                res when res in [:eq, :gt] -> last_updated
                :lt -> entry.updated_at
              end
            end)

          Map.put(acc, entry.series, summary)
        end)
      end)

    res
  end

  @spec list_series(String.t(), Keyword.t()) :: [%Misc{}]
  def list_series(series, opts \\ []) do
    frequency_filter =
      case Keyword.get(opts, :period, :year) do
        :year -> dynamic([x], x.period == "12")
        :month -> dynamic(true)
      end

    date_filter =
      case Keyword.get(opts, :starting_year) do
        nil -> dynamic(true)
        year -> dynamic([x], x.year > ^year or (x.year == ^year and x.period == "12"))
      end

    Misc
    |> where([x], x.series == ^series)
    |> where(^frequency_filter)
    |> where(^date_filter)
    |> order_by(asc: :year, asc: :period)
    |> Repo.all()
  end

  @spec download_series(String.t()) :: :ok | {:error, String.t()}
  def download_series("CPI"), do: download_cpi_series()
  def download_series("HICP"), do: download_hicp_series()

  @doc """
  Download CPI data from the BLS website.  The series id is
  structured.  We're getting the 'SA0' series which is CPI for all
  goods, over the '0000' area which is all urban, with a frequency of
  'R' which is monthly.

  The "annual" CPI is computed from December to December according to
  the historical CPI-U PDF from here:
  https://www.bls.gov/cpi/tables/supplemental-files/home.htm

  Some interesting doc URLs:
  - https://www.bls.gov/developers/
  - https://www.bls.gov/help/hlpforma.htm#CU
  - https://api.bls.gov/publicAPI/v1/timeseries/data/CUUR0000SA0

  The actual query we're using is this:

  ```
  curl -v -X POST -H 'Content-Type: application/json' https://api.bls.gov/publicAPI/v1/timeseries/data/ --data '{"seriesid":["CUUR0000SA0"], "startyear":"2011",  "endyear":"2021"}'
  ```
  """
  @spec download_cpi_series :: :ok | {:error, any()}
  def download_cpi_series do
    Logger.info("Downloading CPI series")

    this_year = Date.utc_today().year
    # If #{first_year}-01-01 is more than 20 years ago, then only
    # results for [#{first_year}-01-01, #{first_year + 20}-01-01] are
    # returned.  So, make sure first_year is only 19 years ago.
    first_year = this_year - 19

    with {:ok, resp} <-
           Finch.build(
             :post,
             "https://api.bls.gov/publicAPI/v1/timeseries/data/",
             [{"Content-Type", "application/json"}],
             Jason.encode!(%{
               "seriesid" => ["CUUR0000SA0"],
               "startyear" => "#{first_year}",
               "endyear" => "#{this_year}",
               "registrationkey" => Application.fetch_env!(:anubis, :bls_api_key)
             })
           )
           |> Finch.request(MyFinch),
         {:ok, json} <- Jason.decode(resp.body) do
      case json do
        %{"Results" => %{"series" => series}} ->
          Enum.find(series, &(&1["seriesID"] == "CUUR0000SA0"))
          |> case do
            %{"data" => entries} ->
              for entry <- entries do
                year = String.to_integer(entry["year"])
                <<"M", period::binary-size(2)>> = entry["period"]

                {:ok, _} =
                  Misc
                  |> where(
                    [x],
                    x.series == @cpi_series_name and x.year == ^year and x.period == ^period
                  )
                  |> Repo.one()
                  |> case do
                    nil ->
                      %Misc{
                        series: @cpi_series_name,
                        data: entry,
                        year: year,
                        period: period
                      }

                    changeset ->
                      changeset
                  end
                  |> Misc.changeset(%{data: entry})
                  |> Repo.insert_or_update()
              end

              :ok

            nil ->
              {:error, "Could not find CUUR0000SA0 series: #{inspect(series)}"}
          end

        _ ->
          {:error, "Could not extract series list from CPI response: #{inspect(json)}"}
      end
    end
  end

  @doc """
  Download HICP data from the ECB Statistical Data Warehouse.

  Interesting links:
  - https://sdw-wsrest.ecb.europa.eu/help/
  - https://sdw.ecb.europa.eu/quickview.do?org.apache.struts.taglib.html.TOKEN=f5f4fb95fb9cd073bb0e7d6cbf2eb79d&SERIES_KEY=122.ICP.M.U2.Y.000000.3.INX&start=01-01-2001&end=&submitOptions.x=0&submitOptions.y=0&trans=N

  The actual query we're trying to use is this:

  ```
  curl -H "Accept: text/csv" https://sdw-wsrest.ecb.europa.eu/service/data/ICP/M.U2.Y.000000.3.INX | less -R
  ```
  """
  @spec download_hicp_series() :: :ok | {:error, String.t()}
  def download_hicp_series do
    Logger.info("Downloading HICP series")

    with {:ok, resp} <-
           Finch.build(
             :get,
             "https://sdw-wsrest.ecb.europa.eu/service/data/ICP/M.U2.Y.000000.3.INX",
             [{"Accept", "text/csv"}]
           )
           |> Finch.request(MyFinch) do
      for [_, _, _, _, _, _, _, period, value | _] <- NimbleCSV.RFC4180.parse_string(resp.body) do
        <<year::binary-size(4), "-", period::binary-size(2)>> = period
        year = String.to_integer(year)
        data = %{year: year, period: period, value: value}

        {:ok, _} =
          Misc
          |> where(
            [x],
            x.series == @hicp_series_name and x.year == ^year and x.period == ^period
          )
          |> Repo.one()
          |> case do
            nil ->
              %Misc{
                series: @hicp_series_name,
                data: data,
                year: year,
                period: period
              }

            changeset ->
              changeset
          end
          |> Misc.changeset(%{data: data})
          |> Repo.insert_or_update()
      end

      :ok
    end
  end
end
