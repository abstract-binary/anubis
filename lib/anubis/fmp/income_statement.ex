defmodule Anubis.FMP.IncomeStatement do
  @moduledoc """
  Schema for the FMP income statement table.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "fmp_income_statements" do
    field :data, :map
    field :ticker, :string

    timestamps()
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(income_statement, attrs) do
    income_statement
    |> cast(attrs, [:ticker, :data])
    |> validate_required([:ticker, :data])
  end

  @spec url_path(String.t()) :: String.t()
  def url_path(ticker) do
    fmp_token = Application.fetch_env!(:anubis, :fmp_token)
    "/api/v3/income-statement/#{ticker}?apikey=#{fmp_token}"
  end
end
