defmodule Anubis.FMP.BalanceSheetStatement do
  @moduledoc """
  Schema for the FMP balance sheet table.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "fmp_balance_sheet_statements" do
    field :data, :map
    field :ticker, :string

    timestamps()
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(balance_sheet_statement, attrs) do
    balance_sheet_statement
    |> cast(attrs, [:ticker, :data])
    |> validate_required([:ticker, :data])
  end

  @spec url_path(String.t()) :: String.t()
  def url_path(ticker) do
    fmp_token = Application.fetch_env!(:anubis, :fmp_token)
    "/api/v3/balance-sheet-statement/#{ticker}?apikey=#{fmp_token}"
  end
end
