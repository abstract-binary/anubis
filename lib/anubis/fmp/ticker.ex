defmodule Anubis.FMP.Ticker do
  @moduledoc """
  Basic metadata about a symbol we can query from FMP.
  """

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:ticker, :string, autogenerate: false}
  @foreign_key_type :string
  schema "fmp_tickers" do
    field :exchange, :string
    field :name, :string

    timestamps()
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(ticker, attrs) do
    ticker
    |> cast(attrs, [:ticker, :name, :exchange])
    |> validate_required([:ticker, :name, :exchange])
    |> unique_constraint(:ticker, name: "fmp_tickers_pkey")
  end

  @spec url_path() :: String.t()
  def url_path do
    fmp_token = Application.fetch_env!(:anubis, :fmp_token)
    "/api/v3/stock/list?apikey=#{fmp_token}"
  end
end
