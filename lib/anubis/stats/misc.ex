defmodule Anubis.Stats.Misc do
  @moduledoc """
  Misc stats downloaded from the web.  We'll document the specific
  series in the `Stats` module.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "stats_misc" do
    field :data, :map
    field :period, :string
    field :series, :string
    field :year, :integer

    timestamps()
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(misc, attrs) do
    misc
    |> cast(attrs, [:data, :period, :series, :year])
    |> validate_required([:data, :period, :series, :year])
  end
end
