defmodule Anubis.IEx do
  @moduledoc """
  IEx helpers for anubis interactive development.
  """

  defmacro __using__(_) do
    quote do
      alias Anubis.FMP
      alias Anubis.Formula
      alias Anubis.Repo
      alias Anubis.Synth
      alias Anubis.Synth.{BasicField, CalcField}
      alias Anubis.Viewer
      alias Anubis.Viewer.{Block, BlockBasicField, BlockCalcField, View}

      import AnubisWeb.Router.Helpers

      import Ecto.Query
    end
  end
end
