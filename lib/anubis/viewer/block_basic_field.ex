defmodule Anubis.Viewer.BlockBasicField do
  @moduledoc """
  Many-to-many join table between Block and BasicField.
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias Anubis.Synth.BasicField
  alias Anubis.Viewer.Block

  @primary_key false
  schema "block_basic_fields" do
    belongs_to :block, Block, primary_key: true, type: :binary_id
    belongs_to :basic_field, BasicField, primary_key: true, type: :binary_id
    field :order, :integer
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(bbf, attrs) do
    bbf
    |> cast(attrs, [:block_id, :basic_field_id, :order])
    |> validate_required([:block_id, :basic_field_id])
  end
end
