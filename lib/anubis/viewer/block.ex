defmodule Anubis.Viewer.Block do
  @moduledoc """
  Schema for a block with text and fields displayed as part of a
  `View`.
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias Anubis.Viewer.{BlockBasicField, BlockCalcField, View}
  alias __MODULE__

  require Logger

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "blocks" do
    field :text, :string
    field :title, :string
    belongs_to :view, View

    has_many :basic_fields, BlockBasicField, on_replace: :delete

    has_many :calc_fields, BlockCalcField, on_replace: :delete

    timestamps()
  end

  @spec changeset(%Block{}, map()) :: Ecto.Changeset.t()
  def changeset(block, attrs) do
    block
    |> cast(attrs, [:text, :title])
    |> validate_required([:title])
  end

  @spec get_field!(%Block{}, binary) :: %BlockBasicField{} | %BlockCalcField{}
  def get_field!(%Block{basic_fields: basic_fields, calc_fields: calc_fields} = block, field_id)
      when is_list(basic_fields) and is_list(calc_fields) do
    (Enum.find(basic_fields, &(&1.basic_field.id == field_id)) ||
       Enum.find(calc_fields, &(&1.calc_field.id == field_id)))
    |> case do
      nil -> raise "Field #{field_id} not found in block #{inspect(block)}"
      x -> x
    end
  end
end
