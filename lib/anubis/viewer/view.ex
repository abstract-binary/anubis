defmodule Anubis.Viewer.View do
  @moduledoc """
  Type describing a view into the underlying data, parametrizable by
  the symbol, years, and maybe other things.
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias Anubis.Viewer.Block

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "views" do
    has_many :blocks, Block, on_replace: :delete
    field :name, :string

    timestamps()
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(view, attrs) do
    view
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> cast_assoc(:blocks)
  end
end
