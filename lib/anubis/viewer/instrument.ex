defmodule Anubis.Viewer.Instrument do
  @moduledoc """
  Ecto schema for the instrument metadata of watched instruments.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "instruments" do
    field :fmp_ticker, :string
    field :symbol, :string
    field :fmp_updated, :naive_datetime

    timestamps()
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(instrument, attrs) do
    instrument
    |> cast(attrs, [:symbol, :fmp_ticker, :fmp_updated])
    |> validate_required([:symbol, :fmp_ticker, :fmp_updated])
  end
end
