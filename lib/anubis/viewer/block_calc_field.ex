defmodule Anubis.Viewer.BlockCalcField do
  @moduledoc """
  Many-to-many join table between Block and CalcField.
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias Anubis.Synth.CalcField
  alias Anubis.Viewer.Block

  @primary_key false
  schema "block_calc_fields" do
    belongs_to :block, Block, primary_key: true, type: :binary_id
    belongs_to :calc_field, CalcField, primary_key: true, type: :binary_id
    field :order, :integer
  end

  @spec changeset(%__MODULE__{}, map()) :: Ecto.Changeset.t()
  def changeset(bbf, attrs) do
    bbf
    |> cast(attrs, [:block_id, :calc_field_id, :order])
    |> validate_required([:block_id, :calc_field_id])
  end
end
