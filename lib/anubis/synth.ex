defmodule Anubis.Synth do
  @moduledoc """
  The Synth context.
  """

  import Ecto.Query, warn: false

  alias Anubis.Repo
  alias Anubis.Synth.BasicField
  alias Anubis.Synth.CalcField
  alias Anubis.Synth.FieldKind
  alias Anubis.Synth.RawTable

  @spec get_field_by_name(String.t()) :: nil | %BasicField{} | %CalcField{}
  def get_field_by_name(name) do
    case Enum.find(list_basic_fields(), &(&1.name == name)) do
      nil -> Enum.find(list_calc_fields(), &(&1.name == name))
      fld -> fld
    end
  end

  @spec get_any_field!(String.t()) :: %BasicField{} | %CalcField{}
  def get_any_field!(id) do
    case get_basic_field(id) do
      {:ok, field} -> field
      {:error, _} -> get_calc_field!(id)
    end
  end

  @spec list_field_kinds() :: [%FieldKind{}]
  def list_field_kinds do
    FieldKind
    |> order_by(asc: :kind)
    |> Repo.all()
  end

  @spec list_calc_fields() :: [%CalcField{}]
  def list_calc_fields do
    CalcField
    |> order_by(asc: :name)
    |> Repo.all()
  end

  @spec get_calc_field!(String.t()) :: %CalcField{}
  def get_calc_field!(id) do
    Repo.get!(CalcField, id)
  end

  @spec get_calc_field(String.t()) :: {:ok, %CalcField{}} | {:error, String.t()}
  def get_calc_field(id) do
    case Repo.get(CalcField, id) do
      nil -> {:error, "CalcField #{id} not found"}
      field -> {:ok, field}
    end
  end

  @spec create_calc_field(map()) :: {:ok, %CalcField{}} | {:error, Ecto.Changeset.t()}
  def create_calc_field(attrs \\ %{}) do
    %CalcField{}
    |> CalcField.changeset(attrs)
    |> Repo.insert()
  end

  @spec update_calc_field(%CalcField{}, map()) ::
          {:ok, %CalcField{}} | {:error, Ecto.Changeset.t()}
  def update_calc_field(%CalcField{} = basic_field, attrs) do
    basic_field
    |> CalcField.changeset(attrs)
    |> Repo.update()
  end

  @spec delete_calc_field(map()) :: {:ok, %CalcField{}} | {:error, Ecto.Changeset.t()}
  def delete_calc_field(%CalcField{} = basic_field) do
    Repo.delete(basic_field)
  end

  @spec change_calc_field(%CalcField{}, map()) :: Ecto.Changeset.t()
  def change_calc_field(%CalcField{} = basic_field, attrs \\ %{}) do
    CalcField.changeset(basic_field, attrs)
  end

  @spec list_raw_tables() :: [%RawTable{}]
  def list_raw_tables do
    RawTable
    |> order_by(asc: :table)
    |> Repo.all()
  end

  @spec get_raw_table!(String.t()) :: %RawTable{}
  def get_raw_table!(table) do
    Repo.get!(RawTable, table)
  end

  @spec get_raw_table(String.t()) :: {:ok, %RawTable{}} | {:error, String.t()}
  def get_raw_table(table) do
    case Repo.get(RawTable, table) do
      nil -> {:error, "RawTable #{table} not found"}
      raw_table -> {:ok, raw_table}
    end
  end

  @spec create_raw_table(map()) :: {:ok, %RawTable{}} | {:error, Ecto.Changeset.t()}
  def create_raw_table(attrs \\ %{}) do
    %RawTable{}
    |> RawTable.changeset(attrs)
    |> Repo.insert()
  end

  @spec update_raw_table(%RawTable{}, map()) :: {:ok, %RawTable{}} | {:error, Ecto.Changeset.t()}
  def update_raw_table(%RawTable{} = raw_table, attrs) do
    raw_table
    |> RawTable.changeset(attrs)
    |> Repo.update()
  end

  @spec delete_raw_table(%RawTable{}) :: {:ok, %RawTable{}} | {:error, Ecto.Changeset.t()}
  def delete_raw_table(%RawTable{} = raw_table) do
    Repo.delete(raw_table)
  end

  @spec change_raw_table(%RawTable{}, map()) :: Ecto.Changeset.t()
  def change_raw_table(%RawTable{} = raw_table, attrs \\ %{}) do
    RawTable.changeset(raw_table, attrs)
  end

  @spec list_basic_fields() :: [%BasicField{}]
  def list_basic_fields do
    BasicField
    |> order_by(asc: :name)
    |> Repo.all()
  end

  @spec get_basic_field!(String.t()) :: %BasicField{}
  def get_basic_field!(id) do
    Repo.get!(BasicField, id)
  end

  @spec get_basic_field(String.t()) :: {:ok, %BasicField{}} | {:error, String.t()}
  def get_basic_field(id) do
    case Repo.get(BasicField, id) do
      nil -> {:error, "BasicField #{id} not found"}
      field -> {:ok, field}
    end
  end

  @spec create_basic_field(map()) :: {:ok, %BasicField{}} | {:error, Ecto.Changeset.t()}
  def create_basic_field(attrs \\ %{}) do
    %BasicField{}
    |> BasicField.changeset(attrs)
    |> Repo.insert()
  end

  @spec update_basic_field(%BasicField{}, map()) ::
          {:ok, %BasicField{}} | {:error, Ecto.Changeset.t()}
  def update_basic_field(%BasicField{} = basic_field, attrs) do
    basic_field
    |> BasicField.changeset(attrs)
    |> Repo.update()
  end

  @spec delete_basic_field(map()) :: {:ok, %BasicField{}} | {:error, Ecto.Changeset.t()}
  def delete_basic_field(%BasicField{} = basic_field) do
    Repo.delete(basic_field)
  end

  @spec change_basic_field(%BasicField{}, map()) :: Ecto.Changeset.t()
  def change_basic_field(%BasicField{} = basic_field, attrs \\ %{}) do
    BasicField.changeset(basic_field, attrs)
  end
end
