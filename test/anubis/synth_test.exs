defmodule Anubis.SynthTest do
  use Anubis.DataCase

  alias Anubis.Synth

  describe "raw_tables" do
    alias Anubis.Synth.RawTable

    @valid_attrs %{
      description: "some description",
      function: "some function",
      module: "some module",
      table: "some_table"
    }
    @update_attrs %{
      description: "some updated description",
      function: "some updated function",
      module: "some updated module",
      table: "some_updated_table"
    }
    @invalid_attrs %{description: nil, function: nil, module: nil, table: nil}

    @spec raw_table_fixture(map()) :: %RawTable{}
    def raw_table_fixture(attrs \\ %{}) do
      {:ok, raw_table} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Synth.create_raw_table()

      raw_table
    end

    test "list_raw_tables/0 returns all raw_tables" do
      raw_table = raw_table_fixture()
      assert Synth.list_raw_tables() == [raw_table]
    end

    test "get_raw_table!/1 returns the raw_table with given id" do
      raw_table = raw_table_fixture()
      assert Synth.get_raw_table!(raw_table.table) == raw_table
    end

    test "create_raw_table/1 with valid data creates a raw_table" do
      assert {:ok, %RawTable{} = raw_table} = Synth.create_raw_table(@valid_attrs)
      assert raw_table.description == "some description"
      assert raw_table.function == "some function"
      assert raw_table.module == "some module"
      assert raw_table.table == "some_table"
    end

    test "create_raw_table/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Synth.create_raw_table(@invalid_attrs)
    end

    test "update_raw_table/2 with valid data updates the raw_table" do
      raw_table = raw_table_fixture()
      assert {:ok, %RawTable{} = raw_table} = Synth.update_raw_table(raw_table, @update_attrs)
      assert raw_table.description == "some updated description"
      assert raw_table.function == "some updated function"
      assert raw_table.module == "some updated module"
      assert raw_table.table == "some_updated_table"
    end

    test "update_raw_table/2 with invalid data returns error changeset" do
      raw_table = raw_table_fixture()
      assert {:error, %Ecto.Changeset{}} = Synth.update_raw_table(raw_table, @invalid_attrs)
      assert raw_table == Synth.get_raw_table!(raw_table.table)
    end

    test "delete_raw_table/1 deletes the raw_table" do
      raw_table = raw_table_fixture()
      assert {:ok, %RawTable{}} = Synth.delete_raw_table(raw_table)
      assert_raise Ecto.NoResultsError, fn -> Synth.get_raw_table!(raw_table.table) end
    end

    test "change_raw_table/1 returns a raw_table changeset" do
      raw_table = raw_table_fixture()
      assert %Ecto.Changeset{} = Synth.change_raw_table(raw_table)
    end
  end

  describe "basic_fields" do
    alias Anubis.Synth.BasicField
    alias Anubis.Synth.RawTable

    @valid_attrs %{
      accessor: "some accessor",
      description: "some description",
      name: "some name",
      table: "some_table",
      kind: :scalar
    }
    @update_attrs %{
      accessor: "some updated accessor",
      description: "some updated description",
      name: "some updated name",
      table: "some_table",
      kind: :year_series
    }
    @invalid_attrs %{accessor: nil, description: nil, name: nil, some_table: nil}

    @raw_table_attrs %{
      description: "some description",
      function: "some function",
      module: "some module",
      table: "some_table"
    }

    setup do
      {:ok, %RawTable{}} = Synth.create_raw_table(@raw_table_attrs)
      :ok
    end

    @spec basic_field_fixture(map()) :: %BasicField{}
    def basic_field_fixture(attrs \\ %{}) do
      {:ok, basic_field} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Synth.create_basic_field()

      basic_field
    end

    test "list_basic_fields/0 returns all basic_fields" do
      basic_field = basic_field_fixture()
      assert Synth.list_basic_fields() == [basic_field]
    end

    test "get_basic_field!/1 returns the basic_field with given id" do
      basic_field = basic_field_fixture()
      assert Synth.get_basic_field!(basic_field.id) == basic_field
    end

    test "create_basic_field/1 with valid data creates a basic_field" do
      assert {:ok, %BasicField{} = basic_field} = Synth.create_basic_field(@valid_attrs)
      assert basic_field.accessor == "some accessor"
      assert basic_field.description == "some description"
      assert basic_field.name == "some name"
      assert basic_field.table == "some_table"
      assert basic_field.kind == :scalar
    end

    test "create_basic_field/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Synth.create_basic_field(@invalid_attrs)
    end

    test "update_basic_field/2 with valid data updates the basic_field" do
      basic_field = basic_field_fixture()

      assert {:ok, %BasicField{} = basic_field} =
               Synth.update_basic_field(basic_field, @update_attrs)

      assert basic_field.accessor == "some updated accessor"
      assert basic_field.description == "some updated description"
      assert basic_field.name == "some updated name"
      assert basic_field.table == "some_table"
      assert basic_field.kind == :year_series
    end

    test "update_basic_field/2 with invalid data returns error changeset" do
      basic_field = basic_field_fixture()
      assert {:error, %Ecto.Changeset{}} = Synth.update_basic_field(basic_field, @invalid_attrs)
      assert basic_field == Synth.get_basic_field!(basic_field.id)
    end

    test "delete_basic_field/1 deletes the basic_field" do
      basic_field = basic_field_fixture()
      assert {:ok, %BasicField{}} = Synth.delete_basic_field(basic_field)
      assert_raise Ecto.NoResultsError, fn -> Synth.get_basic_field!(basic_field.id) end
    end

    test "change_basic_field/1 returns a basic_field changeset" do
      basic_field = basic_field_fixture()
      assert %Ecto.Changeset{} = Synth.change_basic_field(basic_field)
    end
  end
end
