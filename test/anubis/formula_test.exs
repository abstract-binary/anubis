defmodule Anubis.FormulaTest.Helpers do
  defmacro fmp_income_statement(net_income, shares_outstanding, date) do
    quote do
      %{
        module: Anubis.FMP.IncomeStatement,
        ticker: "TEST",
        data: %{
          "netIncome" => unquote(net_income) * 1.0,
          "weightedAverageShsOutDil" => unquote(shares_outstanding),
          "date" => unquote(date) |> Date.to_string()
        }
      }
    end
  end

  defmacro fmp_balance_sheet_statement(total_assets, date) do
    quote do
      %{
        module: Anubis.FMP.BalanceSheetStatement,
        ticker: "TEST",
        data: %{
          "totalAssets" => unquote(total_assets) * 1.0,
          "date" => unquote(date) |> Date.to_string()
        }
      }
    end
  end

  defmacro fmp_cash_flow_statement(cash_flow_from_operations, date) do
    quote do
      %{
        module: Anubis.FMP.CashFlowStatement,
        ticker: "TEST",
        data: %{
          "netCashProvidedByOperatingActivities" => unquote(cash_flow_from_operations) * 1.0,
          "date" => unquote(date) |> Date.to_string()
        }
      }
    end
  end

  defmacro hicp(value, date) do
    quote do
      %{
        module: Anubis.Stats.Misc,
        series: "HICP",
        year: unquote(date).year,
        period: unquote(date).month |> Integer.to_string() |> String.pad_leading(2, "0"),
        data: %{
          "value" => (unquote(value) * 1.0) |> Float.to_string()
        }
      }
    end
  end
end

defmodule Anubis.FormulaTest do
  use Anubis.DataCase

  import Anubis.FormulaTest.Helpers
  alias Anubis.Formula

  describe "tests" do
    alias Anubis.Synth
    alias Anubis.Synth.{BasicField, RawTable}

    @fmp_balance_sheet_statments %{
      table: "fmp_balance_sheet_statements",
      description: "FMP Balance Sheets",
      module: "Anubis.FMP",
      function: "query_balance_sheet_statement"
    }

    @fmp_cash_flow_statments %{
      table: "fmp_cash_flow_statements",
      description: "FMP Cash Flow Statements",
      module: "Anubis.FMP",
      function: "query_cash_flow_statement"
    }

    @fmp_income_statements %{
      table: "fmp_income_statements",
      description: "FMP Income Statements",
      module: "Anubis.FMP",
      function: "query_income_statement"
    }

    @stats_misc %{
      table: "stats_misc",
      description: "Misc Statistics",
      module: "Anubis.Stats",
      function: "query_series"
    }

    @net_income %{
      accessor: "netIncome",
      description: "Net Income",
      name: "net_income",
      table: "fmp_income_statements",
      kind: :year_series
    }

    @shares_outstanding %{
      accessor: "weightedAverageShsOutDil",
      description: "Shares Outstanding (Diluted)",
      name: "shares_outstanding",
      table: "fmp_income_statements",
      kind: :year_series
    }

    @total_assets %{
      accessor: "totalAssets",
      description: "Total Assets",
      name: "total_assets",
      table: "fmp_balance_sheet_statements",
      kind: :year_series
    }

    @cash_flow_from_operations %{
      accessor: "netCashProvidedByOperatingActivities",
      description: "Cash Flow From Operating Activities",
      name: "cash_flow_from_operations",
      table: "fmp_cash_flow_statements",
      kind: :year_series
    }

    @hicp %{
      accessor: "value",
      description: "Eurozone Harmonized Index of Consumer Prices",
      name: "hicp",
      table: "stats_misc",
      kind: :year_series
    }

    @fmp_data [
      fmp_income_statement(100, 10, ~D[2011-12-31]),
      fmp_income_statement(200, 10, ~D[2012-12-31]),
      fmp_income_statement(300, 10, ~D[2013-12-31]),
      fmp_income_statement(400, 10, ~D[2014-12-31]),
      fmp_income_statement(500, 10, ~D[2015-12-31]),
      fmp_income_statement(600, 10, ~D[2016-12-31]),
      fmp_income_statement(700, 10, ~D[2017-12-31]),
      fmp_income_statement(800, 10, ~D[2018-12-31]),
      fmp_income_statement(900, 10, ~D[2019-12-31]),
      fmp_income_statement(1000, 10, ~D[2020-12-31]),
      fmp_balance_sheet_statement(500, ~D[2011-12-31]),
      fmp_balance_sheet_statement(500, ~D[2012-12-31]),
      fmp_balance_sheet_statement(500, ~D[2013-12-31]),
      fmp_balance_sheet_statement(500, ~D[2014-12-31]),
      fmp_balance_sheet_statement(500, ~D[2015-12-31]),
      fmp_balance_sheet_statement(500, ~D[2016-12-31]),
      fmp_balance_sheet_statement(500, ~D[2017-12-31]),
      fmp_balance_sheet_statement(500, ~D[2018-12-31]),
      fmp_balance_sheet_statement(500, ~D[2019-12-31]),
      fmp_balance_sheet_statement(500, ~D[2020-12-31]),
      fmp_cash_flow_statement(10, ~D[2011-12-31]),
      fmp_cash_flow_statement(20, ~D[2012-12-31]),
      fmp_cash_flow_statement(30, ~D[2013-12-31]),
      fmp_cash_flow_statement(40, ~D[2014-12-31]),
      fmp_cash_flow_statement(50, ~D[2015-12-31]),
      fmp_cash_flow_statement(60, ~D[2016-12-31]),
      fmp_cash_flow_statement(70, ~D[2017-12-31]),
      fmp_cash_flow_statement(80, ~D[2018-12-31]),
      fmp_cash_flow_statement(90, ~D[2019-12-31]),
      fmp_cash_flow_statement(100, ~D[2020-12-31]),
      hicp(100.0, ~D[2011-12-31]),
      hicp(102.22, ~D[2012-12-31]),
      hicp(103.15, ~D[2013-12-31]),
      hicp(102.99, ~D[2014-12-31]),
      hicp(103.44, ~D[2015-12-31]),
      hicp(104.59, ~D[2016-12-31]),
      hicp(106.03, ~D[2017-12-31]),
      hicp(107.66, ~D[2018-12-31]),
      hicp(109.10, ~D[2019-12-31]),
      hicp(108.80, ~D[2020-12-31])
    ]

    setup do
      {:ok, %RawTable{}} = Synth.create_raw_table(@fmp_income_statements)
      {:ok, %RawTable{}} = Synth.create_raw_table(@fmp_balance_sheet_statments)
      {:ok, %RawTable{}} = Synth.create_raw_table(@fmp_cash_flow_statments)
      {:ok, %RawTable{}} = Synth.create_raw_table(@stats_misc)

      {:ok, %BasicField{} = net_income_field} = Synth.create_basic_field(@net_income)

      {:ok, %BasicField{} = cash_flow_from_operations_field} =
        Synth.create_basic_field(@cash_flow_from_operations)

      {:ok, %BasicField{} = shares_outstanding_field} =
        Synth.create_basic_field(@shares_outstanding)

      {:ok, %BasicField{} = total_assets_field} = Synth.create_basic_field(@total_assets)

      {:ok, %BasicField{} = hicp_field} = Synth.create_basic_field(@hicp)

      for stmt <- @fmp_data do
        stmt.module.changeset(struct!(stmt.module), stmt)
        |> Repo.insert()
      end

      {:ok,
       %{
         net_income_field: net_income_field,
         shares_outstanding_field: shares_outstanding_field,
         total_assets_field: total_assets_field,
         cash_flow_from_operations_field: cash_flow_from_operations_field,
         hicp_field: hicp_field
       }}
    end

    test "create field formula", env do
      x = Formula.new_field(env.net_income_field.id)
      assert x.kind == :field
      assert x.field == env.net_income_field.id
    end

    test "create calc formula", env do
      x = Formula.new_calc(:/, [env.net_income_field.id, env.shares_outstanding_field.id])
      assert x.kind == :calc
      assert x.calc == {:/, [env.net_income_field.id, env.shares_outstanding_field.id]}
    end

    test "compute valid income statement field formula", env do
      alias Anubis.Formula.Context, as: Ctx
      x = Formula.new_field(env.net_income_field.id)

      assert {:ok,
              %{
                values: %{
                  2011 => 100.0,
                  2012 => 200.0,
                  2013 => 300.0,
                  2014 => 400.0,
                  2015 => 500.0,
                  2016 => 600.0,
                  2017 => 700.0,
                  2018 => 800.0,
                  2019 => 900.0,
                  2020 => 1000
                }
              }} = Formula.compute_for_years(x, %Ctx{years: 2011..2020, symbol: "TEST"})
    end

    test "compute field formula but parkour into hicp instead", env do
      alias Anubis.Formula.Context, as: Ctx

      x = Formula.new_field(env.hicp_field.id, symbol: "HICP")

      assert {:ok,
              %{
                values: %{
                  2011 => 100.0,
                  2012 => 102.22,
                  2013 => 103.15,
                  2014 => 102.99,
                  2015 => 103.44,
                  2016 => 104.59,
                  2017 => 106.03,
                  2018 => 107.66,
                  2019 => 109.1,
                  2020 => 108.8
                }
              }} = Formula.compute_for_years(x, %Ctx{years: 2011..2020, symbol: "TEST"})
    end

    test "compute invalid field formula", env do
      alias Anubis.Formula.Context, as: Ctx
      x = Formula.new_field(env.net_income_field.id)
      assert {:error, _} = Formula.compute_for_years(x, %Ctx{years: 2000..2000, symbol: "TEST"})

      assert {:error, _} =
               Formula.compute_for_years(x, %Ctx{years: 2011..2011, symbol: "BADTEST"})
    end

    test "compute valid balance sheet field formula", env do
      alias Anubis.Formula.Context, as: Ctx
      x = Formula.new_field(env.total_assets_field.id)

      assert {:ok,
              %{
                values: %{
                  2011 => 500.0,
                  2012 => 500.0,
                  2013 => 500.0,
                  2014 => 500.0,
                  2015 => 500.0,
                  2016 => 500.0,
                  2017 => 500.0,
                  2018 => 500.0,
                  2019 => 500.0,
                  2020 => 500.0
                }
              }} = Formula.compute_for_years(x, %Ctx{years: 2011..2020, symbol: "TEST"})
    end

    test "compute valid cash flow field formula", env do
      alias Anubis.Formula.Context, as: Ctx
      x = Formula.new_field(env.cash_flow_from_operations_field.id)

      assert {:ok,
              %{
                values: %{
                  2011 => 10.0,
                  2012 => 20.0,
                  2013 => 30.0,
                  2014 => 40.0,
                  2015 => 50.0,
                  2016 => 60.0,
                  2017 => 70.0,
                  2018 => 80.0,
                  2019 => 90.0,
                  2020 => 100.0
                }
              }} = Formula.compute_for_years(x, %Ctx{years: 2011..2020, symbol: "TEST"})
    end

    test "compute calc div formula", env do
      alias Anubis.Formula.Context, as: Ctx

      x =
        Formula.new_calc(:/, [
          Formula.new_field(env.net_income_field.id),
          Formula.new_field(env.shares_outstanding_field.id)
        ])

      assert {:ok,
              %{
                values: %{
                  2011 => 10.0,
                  2012 => 20.0,
                  2013 => 30.0,
                  2014 => 40.0,
                  2015 => 50.0,
                  2016 => 60.0,
                  2017 => 70.0,
                  2018 => 80.0,
                  2019 => 90.0,
                  2020 => 100.0
                },
                working_symbolic: %{
                  2011 =>
                    "\\dfrac{\\mathrm{net\\_income}[2011]}{\\mathrm{shares\\_outstanding}[2011]}",
                  2012 =>
                    "\\dfrac{\\mathrm{net\\_income}[2012]}{\\mathrm{shares\\_outstanding}[2012]}",
                  2013 =>
                    "\\dfrac{\\mathrm{net\\_income}[2013]}{\\mathrm{shares\\_outstanding}[2013]}",
                  2014 =>
                    "\\dfrac{\\mathrm{net\\_income}[2014]}{\\mathrm{shares\\_outstanding}[2014]}",
                  2015 =>
                    "\\dfrac{\\mathrm{net\\_income}[2015]}{\\mathrm{shares\\_outstanding}[2015]}",
                  2016 =>
                    "\\dfrac{\\mathrm{net\\_income}[2016]}{\\mathrm{shares\\_outstanding}[2016]}",
                  2017 =>
                    "\\dfrac{\\mathrm{net\\_income}[2017]}{\\mathrm{shares\\_outstanding}[2017]}",
                  2018 =>
                    "\\dfrac{\\mathrm{net\\_income}[2018]}{\\mathrm{shares\\_outstanding}[2018]}",
                  2019 =>
                    "\\dfrac{\\mathrm{net\\_income}[2019]}{\\mathrm{shares\\_outstanding}[2019]}",
                  2020 =>
                    "\\dfrac{\\mathrm{net\\_income}[2020]}{\\mathrm{shares\\_outstanding}[2020]}"
                },
                working_numeric: %{
                  2011 => "\\dfrac{100.00}{10}",
                  2012 => "\\dfrac{200.00}{10}",
                  2013 => "\\dfrac{300.00}{10}",
                  2014 => "\\dfrac{400.00}{10}",
                  2015 => "\\dfrac{500.00}{10}",
                  2016 => "\\dfrac{600.00}{10}",
                  2017 => "\\dfrac{700.00}{10}",
                  2018 => "\\dfrac{800.00}{10}",
                  2019 => "\\dfrac{900.00}{10}",
                  2020 => "\\dfrac{1\\,000}{10}"
                }
              }} = Formula.compute_for_years(x, %Ctx{years: 2011..2020, symbol: "TEST"})
    end

    test "compute calc sum formula", env do
      alias Anubis.Formula.Context, as: Ctx

      x =
        Formula.new_calc(:sum, [
          Formula.new_field(env.net_income_field.id)
        ])

      assert {:ok,
              %{
                values: 5500.0,
                working_symbolic:
                  "sum(\\mathrm{net\\_income}[2011], \\mathrm{net\\_income}[2012], \\mathrm{net\\_income}[2013], \\mathrm{net\\_income}[2014], \\mathrm{net\\_income}[2015], \\mathrm{net\\_income}[2016], \\mathrm{net\\_income}[2017], \\mathrm{net\\_income}[2018], \\mathrm{net\\_income}[2019], \\mathrm{net\\_income}[2020])",
                working_numeric:
                  "sum(100.00, 200.00, 300.00, 400.00, 500.00, 600.00, 700.00, 800.00, 900.00, 1\\,000)"
              }} = Formula.compute_for_years(x, %Ctx{years: 2011..2020, symbol: "TEST"})

      assert {:ok,
              %{
                values: 4000.0,
                working_symbolic:
                  "sum(\\mathrm{net\\_income}[2016], \\mathrm{net\\_income}[2017], \\mathrm{net\\_income}[2018], \\mathrm{net\\_income}[2019], \\mathrm{net\\_income}[2020])",
                working_numeric: "sum(600.00, 700.00, 800.00, 900.00, 1\\,000)"
              }} = Formula.compute_for_years(x, %Ctx{years: 2016..2020, symbol: "TEST"})

      assert {:error, _} = Formula.compute_for_years(x, %Ctx{years: 2000..2001, symbol: "TEST"})
    end

    test "compute calc abs formula", env do
      alias Anubis.Formula.Context, as: Ctx

      x =
        Formula.new_calc(:abs, [
          Formula.new_field(env.net_income_field.id)
        ])

      assert {:ok,
              %{
                working_symbolic: %{
                  2011 => "abs(\\mathrm{net\\_income}[2011])",
                  2012 => "abs(\\mathrm{net\\_income}[2012])",
                  2013 => "abs(\\mathrm{net\\_income}[2013])",
                  2014 => "abs(\\mathrm{net\\_income}[2014])",
                  2015 => "abs(\\mathrm{net\\_income}[2015])",
                  2016 => "abs(\\mathrm{net\\_income}[2016])",
                  2017 => "abs(\\mathrm{net\\_income}[2017])",
                  2018 => "abs(\\mathrm{net\\_income}[2018])",
                  2019 => "abs(\\mathrm{net\\_income}[2019])",
                  2020 => "abs(\\mathrm{net\\_income}[2020])"
                },
                working_numeric: %{
                  2011 => "abs(100.00)",
                  2012 => "abs(200.00)",
                  2013 => "abs(300.00)",
                  2014 => "abs(400.00)",
                  2015 => "abs(500.00)",
                  2016 => "abs(600.00)",
                  2017 => "abs(700.00)",
                  2018 => "abs(800.00)",
                  2019 => "abs(900.00)",
                  2020 => "abs(1\\,000)"
                }
              }} = Formula.compute_for_years(x, %Ctx{years: 2011..2020, symbol: "TEST"})
    end
  end
end
