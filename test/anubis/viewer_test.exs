defmodule Anubis.ViewerTest do
  use Anubis.DataCase

  alias Anubis.Viewer

  describe "instruments" do
    alias Anubis.Viewer.Instrument

    @valid_attrs %{
      fmp_ticker: "some fmp_ticker",
      symbol: "some symbol",
      fmp_updated: ~N[2021-05-08 09:00:00]
    }
    @update_attrs %{
      fmp_ticker: "some updated fmp_ticker",
      symbol: "some updated symbol",
      fmp_updated: ~N[2021-05-09 10:00:00]
    }
    @invalid_attrs %{fmp_ticker: nil, symbol: nil, fmp_updated: ""}

    @spec instrument_fixture(map()) :: %Instrument{}
    def instrument_fixture(attrs \\ %{}) do
      {:ok, instrument} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Viewer.create_instrument()

      instrument
    end

    test "list_instruments/0 returns all instruments" do
      instrument = instrument_fixture()
      assert Viewer.list_instruments() == [instrument]
    end

    test "get_instrument!/1 returns the instrument with given id" do
      instrument = instrument_fixture()
      assert Viewer.get_instrument!(instrument.id) == instrument
    end

    test "create_instrument/1 with valid data creates a instrument" do
      assert {:ok, %Instrument{} = instrument} = Viewer.create_instrument(@valid_attrs)
      assert instrument.fmp_ticker == "some fmp_ticker"
      assert instrument.symbol == "some symbol"
    end

    test "create_instrument/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Viewer.create_instrument(@invalid_attrs)
    end

    test "update_instrument/2 with valid data updates the instrument" do
      instrument = instrument_fixture()

      assert {:ok, %Instrument{} = instrument} =
               Viewer.update_instrument(instrument, @update_attrs)

      assert instrument.fmp_ticker == "some updated fmp_ticker"
      assert instrument.symbol == "some updated symbol"
    end

    test "update_instrument/2 with invalid data returns error changeset" do
      instrument = instrument_fixture()
      assert {:error, %Ecto.Changeset{}} = Viewer.update_instrument(instrument, @invalid_attrs)
      assert instrument == Viewer.get_instrument!(instrument.id)
    end

    test "delete_instrument/1 deletes the instrument" do
      instrument = instrument_fixture()
      assert {:ok, %Instrument{}} = Viewer.delete_instrument(instrument)
      assert_raise Ecto.NoResultsError, fn -> Viewer.get_instrument!(instrument.id) end
    end

    test "change_instrument/1 returns a instrument changeset" do
      instrument = instrument_fixture()
      assert %Ecto.Changeset{} = Viewer.change_instrument(instrument)
    end
  end

  describe "views" do
    alias Anubis.Viewer.View

    @valid_attrs %{blocks: [], name: "some name"}
    @update_attrs %{
      blocks: [%{title: "some title", text: "some text"}],
      name: "some updated name"
    }
    @invalid_attrs %{blocks: nil, name: nil}

    @spec view_fixture(map()) :: %View{}
    def view_fixture(attrs \\ %{}) do
      {:ok, view} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Viewer.create_view()

      view
    end

    test "list_views/0 returns all views" do
      view = view_fixture()
      assert Viewer.list_views() == [view]
    end

    test "get_view!/1 returns the view with given id" do
      view = view_fixture()
      assert Viewer.get_view!(view.id) == view
    end

    test "create_view/1 with valid data creates a view" do
      assert {:ok, %View{} = view} = Viewer.create_view(@valid_attrs)
      assert view.blocks == []
      assert view.name == "some name"
    end

    test "create_view/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Viewer.create_view(@invalid_attrs)
    end

    test "update_view/2 with valid data updates the view" do
      view = view_fixture()
      assert {:ok, %View{} = view} = Viewer.update_view(view, @update_attrs)
      assert [%{}] = view.blocks
      assert view.name == "some updated name"
    end

    test "update_view/2 with invalid data returns error changeset" do
      view = view_fixture()
      assert {:error, %Ecto.Changeset{}} = Viewer.update_view(view, @invalid_attrs)
      assert view == Viewer.get_view!(view.id)
    end

    test "delete_view/1 deletes the view" do
      view = view_fixture()
      assert {:ok, %View{}} = Viewer.delete_view(view)
      assert_raise Ecto.NoResultsError, fn -> Viewer.get_view!(view.id) end
    end

    test "change_view/1 returns a view changeset" do
      view = view_fixture()
      assert %Ecto.Changeset{} = Viewer.change_view(view)
    end
  end
end
