.PHONY: clean

clean:
	rm -rf assets/node_modules/
	rm -rf assets/vendor/
	rm -rf dist/
	rm -rf lib/anubis_web/controllers/
	rm -rf priv/static/
