module.exports = {
  ident: 'postcss',
  plugins: {
      tailwindcss: {},
      "postcss-color-function": {},
      "postcss-extend-rule": {},
      "postcss-advanced-variables": {},
      "postcss-preset-env": {},
      "postcss-atroot": {},
      "postcss-property-lookup": {},
      "postcss-nested": {},
      autoprefixer: {},
  }
}
